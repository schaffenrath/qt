import "../output"
import QtQuick 2.7
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

import Process 1.0

ListView {
    id: processView
    //implicitHeight: parent.height-10
    anchors.bottom: parent.bottom
    //implicitWidth: parent.width
    clip: true

    property bool lsActive: processController.getButtonState(ProcessModel.LS)
    property bool psActive: processController.getButtonState(ProcessModel.PSnon)
    property bool changed: false

    signal clicked()

    model: ProcessModel {
        list: processController
    }
    spacing: 10
    delegate: RowLayout {
        property int procIndex: index
        anchors.left: parent.left
        width: parent.width
        height: 30

        OwnText {
            id: processId
            text: "P"+model.id
            width: 50
            color: "#525252"
            font.pixelSize: 16
            anchors.left: parent.left
            anchors.leftMargin: parent.width/20
        }

        TextField {
            id: processInsertion
            Layout.preferredWidth: 50
            Layout.preferredHeight: 30
            horizontalAlignment: TextInput.AlignRight
            color: "#525252"
            selectByMouse: true
            maximumLength: 4
            inputMethodHints: Qt.ImhDigitsOnly
            font.family: "Ubuntu"
            font.pixelSize: 16
            anchors.left: parent.left
            anchors.leftMargin: parent.width/7
            placeholderText: "0"
            text: model.insertion

            onActiveFocusChanged: activeFocus ? selectAll() : deselect()

            onTextEdited: {
                model.insertion = text
                model.insertion = text
            }
        }

        TextField {
            id: processDuration
            Layout.preferredWidth: 50
            Layout.preferredHeight: 30
            horizontalAlignment: TextInput.AlignRight
            color: "#525252"
            selectByMouse: true
            maximumLength: 4
            inputMethodHints: Qt.ImhDigitsOnly
            font.family: "Ubuntu"
            font.pixelSize: 16
            anchors.left: parent.left
            anchors.leftMargin: 2*(parent.width/7)
            placeholderText: "0"
            text: model.duration
            onActiveFocusChanged: activeFocus ? selectAll() : deselect()


            onTextEdited: {
                model.duration = text
                model.duration = text
            }
        }

        TextField {
            id: processPriority
            Layout.preferredWidth: 50
            Layout.preferredHeight: 30
            horizontalAlignment: TextInput.AlignRight
            color: "#525252"
            selectByMouse: true
            maximumLength: 4
            inputMethodHints: Qt.ImhDigitsOnly
            font.family: "Ubuntu"
            font.pixelSize: 16
            anchors.left: parent.left
            anchors.leftMargin: 3*(parent.width/7)
            placeholderText: "0"
            text: model.priority

            onActiveFocusChanged: activeFocus ? selectAll() : deselect()

            onTextEdited: {
                model.priority = text
                model.priority = text
            }
            visible: psActive
        }

        TextField {
            id: processTickets
            Layout.preferredWidth: 50
            Layout.preferredHeight: 30
            horizontalAlignment: TextInput.AlignRight
            color: "#525252"
            selectByMouse: true
            maximumLength: 4
            inputMethodHints: Qt.ImhDigitsOnly
            font.family: "Ubuntu"
            font.pixelSize: 16
            anchors.left: parent.left
            anchors.leftMargin: 4*(parent.width/7)
            placeholderText: "0"
            text: model.tickets
            onActiveFocusChanged: activeFocus ? selectAll() : deselect()

            onTextEdited: {
                model.tickets = text
                model.tickets = text
            }
            visible: lsActive
        }

        TextField {
            id: processQuantumUsage
            Layout.preferredWidth: 50
            Layout.preferredHeight: 30
            horizontalAlignment: TextInput.AlignRight
            color: "#525252"
            selectByMouse: true
            maximumLength: 4
            inputMethodHints: Qt.ImhDigitsOnly
            font.family: "Ubuntu"
            font.pixelSize: 16
            anchors.left: parent.left
            anchors.leftMargin: 5*(parent.width/7)+30
            placeholderText: "0"
            text: model.quantumUsage
            onActiveFocusChanged: activeFocus ? selectAll() : deselect()

            onTextEdited: {
                model.quantumUsage = text
                model.quantumUsage = text
            }
            visible: lsActive
        }


        Rectangle {
            id: removeButton
            height: 20
            width: 20
            anchors.right: parent.right
            anchors.rightMargin: 40
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 8

            Rectangle {
                anchors.centerIn: parent
                width: parent.width+2
                height: 2
                color: "#cc0000"
                rotation: 45
            }

            Rectangle {
                anchors.centerIn: parent
                width: parent.width+2
                height: 2
                color: "#cc0000"
                rotation: -45
            }

            MouseArea {
                anchors.fill: parent
                cursorShape: Qt.PointingHandCursor
                onClicked: {
                    processController.removeProc(procIndex)
                    processView.clicked()
                }
            }
        }
    }

    ScrollBar.vertical: ScrollBar {
        active: true
    }
}
