import QtQuick 2.0
import QtQuick.Controls 2.2

Item {
    id: randInput
    width: parent.width/5
    height: parent.height/2 + parent.height/5
    anchors.top: randInputView.top

    property alias headerText: randInputHead.text
    property alias valueText: randProcNum.text
    property alias maxValue: randProcNum.maximumLength
    signal editingFinished()

    Text {
        id: randInputHead
        color: "#525252"
        font.family: "Ubuntu"
        font.pixelSize: 14
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.topMargin: 10
    }

    TextField {
        id: randProcNum
        height: 30
        width: 40
        anchors.top: randInputHead.bottom
        anchors.topMargin: 10
        anchors.horizontalCenter: randInput.horizontalCenter
        selectByMouse: true
        maximumLength: 3
        inputMethodHints: Qt.ImhDigitsOnly
        validator: IntValidator{bottom: 0}
        horizontalAlignment: TextInput.AlignRight
        font.family: "Ubuntu"
        font.pixelSize: 16
        placeholderText: "0"
        onActiveFocusChanged: activeFocus ? selectAll() : deselect()
        onEditingFinished: {
            randInput.editingFinished()
            focus = false
        }
    }
}
