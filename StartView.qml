import "./buttons"
import "./output"
import QtQuick 2.0
import QtQuick.Controls 2.2

Page {
    id: proc
    width: main.width
    height: main.height
    title: qsTr("Process Scheduling")

    OwnText {
        id: mainHeading
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.topMargin: 40
        text: qsTr("Operating System Services")
        font.bold: true
        font.pixelSize: 38
        font.underline: true
    }


    Row {
        id: row2
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: mainHeading.bottom
        anchors.topMargin: 100
        spacing: 10

        StartSelectButton {
            id: schedulingButton
            width: 300
            height: 300
            anchors.top: parent.top
            source: "assets/scheduling-icon.png"
            text: qsTr("Process Scheduling")
            onClicked: stackView.push("Scheduling.qml")
        }

        StartSelectButton {
            id: deadlockButton
            width: 300
            height: 300
            source: "assets/deadlock-icon.png"
            text: qsTr("Deadlock Avoidance")
            onClicked: stackView.push("Deadlock.qml")
        }

        StartSelectButton {
            id: memoryButton
            width: 300
            height: 300
            source: "assets/memory-icon.png"
            text: qsTr("Memory")
        }

    }

    Rectangle {
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: 10
        width: 100
        height: 30
        border.color: "#ff4747"
        border.width: 1.5
        radius: 1

        MouseArea {
            id: quitArea
            anchors.fill: parent
            cursorShape: Qt.PointingHandCursor
            onClicked: Qt.quit()
        }

        OwnText {
            id: quitLabel
            anchors.centerIn: parent
            text: qsTr("Quit")
            color: "#ff4747"
            font.bold: true
            font.letterSpacing: 1.1
        }
    }

}
