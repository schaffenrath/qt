import "../output"
import QtQuick 2.0

Rectangle {
    id: button
    border.color: "#828282"
    border.width: 1

    property alias text: label.text
    property bool active: false
    signal toggled

    color: active ? "#828282" : "#ffffff"

    OwnText {
        id: label
        anchors.centerIn: parent
        anchors.horizontalCenterOffset: label.text.localeCompare("Lottery Scheduling (LS)") ? 0 : -35
        elide: Text.ElideMiddle
        font.letterSpacing: 1.1
        font.bold: true
        color: active ? "#ffffff" : "#828282"
    }

    MouseArea {
        id: buttonMouseArea
        anchors.fill: parent
        cursorShape: Qt.PointingHandCursor
        hoverEnabled: true

        onClicked: {
            button.toggled()
        }
    }

}
