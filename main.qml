import QtQuick 2.0
import QtQuick.Controls 2.3
import QtQuick.Window 2.2

ApplicationWindow {
    id: main

    visible: true
    minimumWidth: 880
    minimumHeight: 720
    width: 960
    height: 720
    title: qsTr("OS Simulator")
    flags: Qt.Window

    property alias stackView: stackView
    property alias main : main
    property int dpi: Screen.pixelDensity


    StackView {
        id: stackView
        initialItem: "StartView.qml"
        anchors.fill: parent
    }
}
