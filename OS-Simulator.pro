QT += quick
CONFIG += c++11 static

INCLUDEPATH += $$PWD

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += main.cpp \
    src/scheduling/processmodel.cpp \
    src/scheduling/processcontroller.cpp \
    src/scheduling/generate_processes.cpp \
    src/scheduling/generate_scheduling.cpp \
    src/scheduling/metric.cpp \
    src/scheduling/process.cpp \
    src/scheduling/schedule_functions.cpp \
    src/scheduling/scheduling.cpp \
    src/deadlock/calculate.cpp \
    src/deadlock/controller.cpp \
    src/scheduling/event_list.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
RC_ICONS = icon.ico

HEADERS += \
    src/scheduling/processmodel.h \
    src/scheduling/processcontroller.h \
    src/scheduling/generate_processes.h \
    src/scheduling/generate_scheduling.h \
    src/scheduling/metric.h \
    src/scheduling/process.h \
    src/scheduling/schedule_functions.h \
    src/scheduling/scheduling.h \
    src/deadlock/calculate.h \
    src/deadlock/controller.h \
    src/deadlock/banker_process.h \
    src/scheduling/event_list.h
