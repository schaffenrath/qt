#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>

#include "src/scheduling/processcontroller.h"
#include "src/scheduling/processmodel.h"
#include "src/deadlock/controller.h"

int main(int argc, char *argv[])
{
#if defined(Q_OS_WIN)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif

    QGuiApplication app(argc, argv);

    qmlRegisterType<ProcessModel>("Process", 1, 0, "ProcessModel");
    qmlRegisterUncreatableType<ProcessController>("Process", 1, 0, "ProcessController",
                                                  QStringLiteral("ProcessController should not be created in QML"));    
    qmlRegisterUncreatableType<ProcessController>("Process", 1, 0, "Controller",
                                                  QStringLiteral("Controller should not be created in QML"));

    ProcessController processController;
    Controller controller;

    QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty(QStringLiteral("processController"), &processController);
    engine.rootContext()->setContextProperty(QStringLiteral("controller"), &controller);
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
