#include <vector>
#include <iostream>
#include <QtCore>
#include <QVector>

#include "controller.h"

Controller::Controller(QObject *parent) : QObject(parent)
{
    resourceColors.push_back("#512e5f");
    resourceColors.push_back("#145a32");
    resourceColors.push_back("#6e2c00");
    resourceColors.push_back("#1b4f72");
    resourceColors.push_back("#641e16");
    resourceColors.push_back("#0e6251");
    resourceColors.push_back("#594f4f");
    resourceColors.push_back("#03396c");
    resourceColors.push_back("#49657b");

    if (numOfResources == 0)
    {
        addResource();
        setNumOfProcs("1");
    }
}

QVector<banker_process_t> Controller::getProcList() const
{
    return procList;
}

int Controller::getNumOfProcs()
{
    return procList.size();
}

int Controller::getNumOfResources()
{
    if (numOfResources != available.size())
    {
        if (numOfResources > available.size())
            for (int i=numOfResources; i > available.size(); i--)
                available.push_back(0);
        else
            for (int i=available.size(); i < numOfResources; i++)
                available.pop_back();
    }

    return numOfResources;
}

void Controller::clearInput()
{
    for (int i=0; i < procList.size(); i++)
    {
        for (int j=0; j < numOfResources; j++)
        {
            procList[i].allocation[j] = 0;
            procList[i].max [j]= 0;
            procList[i].need[j] = 0;
        }
    }

    for (int j=0; j < numOfResources; j++)
    {
        available[j] = 0;
        allocationSum[j] = 0;
        allocationTotal[j] = 0;
    }
    emit dataChanged();
}

QString Controller::getResourceColor(int index)
{
        if (index == -1)
            return QString("#ffffff");

        if (index < resourceColors.size())
            return resourceColors.at(index);

        return resourceColors[index%resourceColors.size()];
}

void Controller::addResource()
{
    for (int i=0; i < procList.size(); i++)
    {
        procList[i].allocation.push_back(0);
        procList[i].max.push_back(0);
        procList[i].need.push_back(0);
    }
    available.push_back(0);
    allocationSum.push_back(0);
    allocationTotal.push_back(0);
    numOfResources++;
    emit dataChanged();
}

void Controller::removeResource()
{
    for (banker_process_t proc : procList)
    {
        proc.allocation.removeLast();
        proc.max.pop_back();
        proc.need.pop_back();
    }
    available.pop_back();
    allocationSum.pop_back();
    allocationTotal.pop_back();
    numOfResources--;
    emit dataChanged();
}

void Controller::setAllocationAt(int procIndex, int resourceIndex, int value)
{
    if (procIndex >= procList.size() || resourceIndex >= numOfResources)
        return;

    if (value != procList[procIndex].allocation[resourceIndex])
    {
        int oldValue = procList.at(procIndex).allocation.at(resourceIndex);
        allocationSum[resourceIndex] -= oldValue;
        allocationTotal[resourceIndex] -= oldValue;
        procList[procIndex].max[resourceIndex] -= oldValue;
        allocationSum[resourceIndex] += value;
        procList[procIndex].allocation[resourceIndex] = value;
        allocationTotal[resourceIndex] += value;
        procList[procIndex].max[resourceIndex] += value;
        //emit dataChanged();
    }
}

void Controller::setNeedAt(int procIndex, int resourceIndex, int value)
{
    if (procIndex >= procList.size() || resourceIndex >= numOfResources)
        return;

    if (value != procList[procIndex].need[resourceIndex])
    {
        procList[procIndex].need[resourceIndex] = value;
        procList[procIndex].max[resourceIndex] = value + procList[procIndex].allocation[resourceIndex];
        //emit dataChanged();
    }
}

void Controller::setMaxAt(int procIndex, int resourceIndex, int value)
{
    if (procIndex >= procList.size() || resourceIndex >= numOfResources)
        return;

    if (value <  procList[procIndex].allocation[resourceIndex] && value != 0)
    {
        procList[procIndex].allocation[resourceIndex] = value;
        emit dataChanged();
        return;
    }

    procList[procIndex].max[resourceIndex] = value;
    procList[procIndex].need[resourceIndex] = value - procList[procIndex].allocation[resourceIndex];
}

bool Controller::isNewAllocationTotalValid(int resourceIndex, int value)
{
    if (resourceIndex >= numOfResources)
        return false;
    if (value - allocationSum.at(resourceIndex) < 0)
        return false;
    return true;
}

void Controller::setAllocationTotalAt(int resourceIndex, int value)
{
    if (resourceIndex >= numOfResources)
        return;

    allocationTotal[resourceIndex] = value;
    available[resourceIndex] = value - allocationSum[resourceIndex];
    //emit dataChanged();
}

int Controller::getAllocationAt(int procIndex, int resourceIndex)
{
    if (procIndex >= procList.size() || resourceIndex >= numOfResources)
        return -1;

    return procList.at(procIndex).allocation.at(resourceIndex);
}

int Controller::getNeedAt(int procIndex, int resourceIndex)
{
    if (procIndex >= procList.size() || resourceIndex >= numOfResources)
        return -1;

    return procList.at(procIndex).need.at(resourceIndex);
}

int Controller::getMaxAt(int procIndex, int resourceIndex)
{
    if (procIndex >= procList.size() || resourceIndex >= numOfResources)
        return -1;

    return procList.at(procIndex).max.at(resourceIndex);
}

bool Controller::isMaxValid(int procIndex, int resourceIndex, int value)
{
    if (procIndex >= procList.size() || resourceIndex >= numOfResources)
        return false;
    if (procList.at(procIndex).allocation.at(resourceIndex) > value)
        return false;
    return true;
}

int Controller::getAllocationTotalAt(int resourceIndex)
{
    if (resourceIndex >= numOfResources)
        return -1;

    return allocationTotal.at(resourceIndex);
}

int Controller::getAllocationSumAt(int resourceIndex)
{
    if (resourceIndex >= numOfResources)
        return -1;
    return allocationSum.at(resourceIndex);
}

bool Controller::isAllocationTotalValid(int index)
{
    if (index >= numOfResources)
        return false;

    if (allocationTotal[index] == allocationSum[index] + available[index])
        return true;

    return false;
}

void Controller::toggleNeed()
{
    needSelected = !needSelected;
    emit dataChanged();
}

bool Controller::isNeedSelected()
{
    return needSelected;
}

void Controller::setAvailableAt(int index, int value)
{
    if (index >= available.size())
        return;

    available[index] = value;

    if (value + allocationSum[index] != allocationTotal[index])
    {
        allocationTotal[index] = value + allocationSum[index];
    }
}

int Controller::getAvailableAt(int index)
{
    if (index >= available.size())
            return -1;
    return available.at(index);
}

void Controller::calcSequences()
{
    sequences.clear();
    calc_solutions(&procList, available, &sequences);
    emit dataChanged();
}

int Controller::getNumOfSequences()
{
    return sequences.size();
}

int Controller::getSeqResultAt(int resultIndex, int posIndex)
{
    if (resultIndex >= sequences.size() || posIndex >= sequences.at(resultIndex).size())
        return -1;

    return sequences.at(resultIndex).at(posIndex)->id;
}

bool Controller::isErrorActive()
{
    return error;
}

void Controller::setError(bool state)
{
    error = state;
    emit dataChanged();
}

void Controller::setErrorHeading(QString heading)
{
    errorHeading = heading;
}

void Controller::setErrorDescription(QString description)
{
    errorDescription = description;
}

QString Controller::getErrorHeading()
{
    return errorHeading;
}

QString Controller::getErrorDescription()
{
    return errorDescription;
}

void Controller::setSequenceIndex(int index)
{
    selectedSequence = index;
}

int Controller::getSequenceIndex()
{
    return selectedSequence;
}

int Controller::getStepNeed(int procIndex, int posIndex)
{
    if (sequences.empty())
        return -1;
    if (selectedSequence >= sequences.size() || procIndex >= procList.size() || posIndex >= numOfResources)
        return -1;

    return sequences.at(selectedSequence).at(procIndex)->need.at(posIndex);
}

void Controller::calcSteps()
{
    if (selectedSequence >= sequences.size())
        return;

    steps.clear();
    calc_steps(available, &sequences[selectedSequence], &steps);
}

int Controller::getStepAt(int lineIndex, int posIndex)
{
    if (steps.empty())
        return -1;
    if (lineIndex >= steps.size() || posIndex >= numOfResources)
        return -1;

    return steps.at(lineIndex).available.at(posIndex);
}

int Controller::getStepId(int lineIndex)
{
    if (steps.empty())
        return -1;
    if (lineIndex >= steps.size())
        return -1;

    return steps.at(lineIndex).proc->id;
}

void Controller::changeButtonState()
{
    buttonStateChanged = true;
}

bool Controller::buttonChanged()
{
    if (buttonStateChanged)
    {
        buttonStateChanged = false;
        return true;
    }
    return false;
}

void Controller::findMinAvail()
{
    calc_min_available(&procList, &available);
}

int Controller::getGenerateProcAmount()
{
    return generate_proc_amount;
}

void Controller::setGenerateResourceAmount(int amount)
{
    generate_resource_amount = amount;
}

bool Controller::isGenerationAttributesValid()
{
    if (generate_proc_amount == 0 || generate_resource_amount == 0)
        return false;
    return true;
}

QString Controller::getGenerateErrorMessage()
{
    if (generate_proc_amount == 0 && generate_resource_amount == 0)
        return QString("Invalid values for #Processes and #Resources. Must be greater than 0");
    else if (generate_proc_amount == 0)
        return QString("Invalid value for #Processes. Must be greater than 0");
    else if (generate_resource_amount == 0)
        return QString("Invalid value for #Resources. Must be greater than 0");
    else
        return QString("Undefined error");
}

void Controller::generateProcs()
{
    procList.clear();
    generate_procs(&procList, &available, &allocationSum, &allocationTotal, generate_proc_amount, generate_resource_amount);
    numOfResources = generate_resource_amount;
    emit dataChanged();
}

int Controller::getGenerateResourceAmount()
{
    return generate_resource_amount;
}

void Controller::setGenerateProcAmount(int amount)
{
    generate_proc_amount = amount;
}

void Controller::appendProc()
{
    emit preProcAppend();

    banker_process_t new_proc;
    new_proc.id = maxProcIdBanker;

    for (int i=0; i < numOfResources; i++)
    {
        new_proc.allocation.push_back(0);
        new_proc.max.push_back(0);
        new_proc.need.push_back(0);
    }

    procList.push_back(new_proc);
    maxProcIdBanker++;
    emit postProcAppend();
    emit dataChanged();
}

void Controller::popProc()
{
    if (procList.size() == 1)
        return;

    for (int i=0; i < numOfResources; i++)
    {
        allocationSum[i] -= procList.last().allocation.at(i);
        allocationTotal[i] -= procList.last().allocation.at(i);
    }
    procList.pop_back();
    maxProcIdBanker--;
    emit dataChanged();
}

void Controller::setNumOfProcs(QString amountStr)
{
    int amount = amountStr.toInt();
    if (amount > 100)
        return;

    if (amount > procList.size())
    {
        for (int i=0; i<amount; i++)
        {
            banker_process_t new_proc;
            new_proc.id = maxProcIdBanker;
            for (int j=0; j < numOfResources; j++)
            {
                new_proc.allocation.push_back(0);
                new_proc.max.push_back(0);
                new_proc.need.push_back(0);
            }
            procList.push_back(new_proc);
            maxProcIdBanker++;
        }
    }

    else if (amount < procList.size())
    {
        for (int i=procList.size(); i > amount; i--)
        {
            for (int j=0; j < numOfResources; j++)
            {
                allocationSum[j] -= procList.last().allocation.at(j);
                allocationTotal[j] -= procList.last().allocation.at(j);
            }
            procList.pop_back();
        }
    }
    emit dataChanged();
}
