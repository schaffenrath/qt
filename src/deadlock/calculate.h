/*
 * Author: Robert Schaffenrath
 * Supervisor: Dr. Thomas Fahringer
 *
 * University of Innsbruck, Institute of Computer Science, http://dps.uibk.ac.at
 */

#ifndef CALCULATE_H
#define CALCULATE_H

#include "banker_process.h"
#include <QVector>

typedef QVector<QVector<const banker_process_t*>> result_seq_t;

typedef struct result_step
{
  const banker_process_t *proc;
  QVector<int> available;
} result_step_t;

typedef QVector<result_step_t> result_steps_t;


void calc_solutions(QVector<banker_process_t> *data, QVector<int> available, result_seq_t *sequences);

void calc_steps(QVector<int> available, QVector<const banker_process_t*> *sequence, result_steps_t *steps);

void generate_procs(QVector<banker_process_t> *data, QVector<int> *available, QVector<int> *allocationSum, QVector<int> *allocationTotal, int proc_amount, int resource_amount);

void calc_min_available(QVector<banker_process_t> *data, QVector<int> *available);

#endif
