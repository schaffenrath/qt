#include "calculate.h"
#include <iostream>
#include <chrono>
#include <random>
#include <limits>

#include <QDebug>

using namespace std;

bool has_lower_id(banker_process_t x, banker_process_t y)
{
    return x.id < y.id;
}

void find_sequence(QVector<banker_process_t> *data, QVector<int> available, bool used[], QVector<const banker_process_t*> found, result_seq_t *sequences)
{
    //iterate over every process in the process list
    for (int i=0; i < data->size(); i++)
    {
        //if a process has not been marked as used
        if (!used[i])
        {
            bool is_valid = true;
            //check for every resource type if more available resources exist than are needed
            for (int j=0; j < available.size(); j++)
            {
                //if the needs can not be fulfilled, break
                if (available.at(j) < data->at(i).need.at(j))
                {
                    is_valid = false;
                    break;
                }
            }

            //if more resources are available than are needed by a process
            if (is_valid)
            {
                //mark process as used
                used[i] = true;
                //add the allocation of the process to the available resources
                for (int j=0; j < available.size(); j++)
                    available[j] += data->at(i).allocation.at(j);

                //add the safe process to the found list
                found.append(&data->at(i));
                //recursive call of function to search for another process
                find_sequence(data, available, used, found, sequences);
                //remove the safe process from the found list to test another combination
                found.pop_back();
                //unmark process
                used[i] = false;

                //subtract the allocation of the process from the available resources
                for (int j=0; j < available.size(); j++)
                    available[j] -= data->at(i).allocation.at(j);
            }
        }
    }

    /* when every process from the process_list is contained in the found_process_list,
     * a safe sequence is added to the found_process_list
     */
    if (found.size() == data->size())
        sequences->append(found);
}


void calc_solutions(QVector<banker_process_t> *data, QVector<int> available, result_seq_t *sequences)//, result_sequences *valid_sequences, QVector<result_step> *calculations)
{
    if (data->empty())
        return;

    //assigning the size to a const type for the allocation of the used_processes array
    const int used_size = data->size();
    //bool array used to keep track already used processes in sequence
    bool *used = new bool[used_size];
    //initializing bool array
    for (int i=0; i < data->size(); i++)
        used[i] = false;
    QVector<const banker_process_t*> found;
    find_sequence(data, available, used, found, sequences);
    delete[] used;
}

void calc_steps(QVector<int> available, QVector<const banker_process_t*> *sequence, result_steps_t *steps)
{
    result_step_t step;

    for (int i=0; i < sequence->size(); i++)
    {
        step.proc = sequence->at(i);
        step.available = available;
        steps->push_back(step);

        for (int j=0; j < available.size(); j++)
            available[j] += step.proc->allocation.at(j);
    }
}

void generate_procs(QVector<banker_process_t> *data, QVector<int> *available,  QVector<int> *allocationSum, QVector<int> *allocationTotal, int proc_amount, int resource_amount)
{
    mt19937 gen(chrono::system_clock::now().time_since_epoch().count());
    //if the function is called reset all data to generate new data
    data->clear();
    available->clear();
    allocationSum->clear();
    allocationTotal->clear();
    //define fix steps
    int max = 4;
    if (resource_amount < 3 && proc_amount > 5)
        max = 8;

    //create list for selection of unique process ids
    QVector<int> available_ids;
    for (int i=0; i < proc_amount; i++)
        available_ids.append(i);

    uniform_real_distribution<double> random_value(0, (double) max);
    bool first = true;

    for (int created = 0; created < proc_amount; created++)
    {
        //create new process
        banker_process_t new_proc;
        uniform_real_distribution<double> random_id(0, (double) available_ids.size());
        int pos = random_id(gen);
        //select random id and remove it from possible id list
        new_proc.id = available_ids[pos];
        available_ids.remove(pos);

        //if its the first iteration
        if (first)
        {
            //generate available resources and needed resources for first process
            for (int i=0; i < resource_amount; i++)
            {
                int avail_value = random_value(gen);
                available->append(avail_value);
                new_proc.need.append(avail_value > 0 && random_value(gen) > 1 ? avail_value - 1 : avail_value);
                //add generated available resources to allocationTotal and initialize allocationSum
                allocationTotal->append(avail_value);
                allocationSum->append(0);

            }
            //indicate that the first process has been generated
            first = false;
            data->append(new_proc);
            continue;
        }

        for (int i=0; i < resource_amount; i++)
        {
            //generate need for new process
            int new_value = data->last().need.at(i) + random_value(gen);

            //add more randomness to the generated need value
            if (new_value <= allocationTotal->at(i))
                new_value += (allocationTotal->at(i) - new_value) + 1;

            new_proc.need.append(new_value);
            //calculate the allocation for the previously generated process
            int new_alloc = new_value - allocationTotal->at(i) + random_value(gen);
            data->last().allocation.append(new_alloc);
            //update allocationTotal and allocationSum
            (*allocationTotal)[i] += new_alloc;
            data->last().max.append(data->last().allocation.at(i) + data->last().need.at(i));
            (*allocationSum)[i] += data->last().allocation.at(i);

        }

        //if its the last generated process the allocation can be arbitrary
        if (created == proc_amount - 1)
        {
            for (int i=0; i < resource_amount; i++)
            {
                int new_alloc = random_value(gen);
                new_proc.allocation.append(new_alloc);
                (*allocationTotal)[i] += new_alloc;
                (*allocationSum)[i] += new_alloc;
                new_proc.max.append(new_proc.allocation.at(i) + new_proc.need.at(i));
            }
        }

        data->append(new_proc);
    }
    //sort the generated processes after the id
    sort(data->begin(), data->end(), has_lower_id);
}

void find_min(QVector<banker_process_t> *data, QVector<int> *min_avail, QVector<int> *needed_avail, QVector<int> *avail, bool used[], int found)
{
    //iterate over all processes
    for (int i=0; i < data->size(); i++)
    {
        //if the process is not marked as used
        if (!used[i])
        {
            //mark it as used
            used[i] = true;
            /* this vector is used to keep track of the available resources in between the
             * recursive function calls
             */
            QVector<int> avail_dif;
            //initialize available difference vector
            for (int j=0; j < min_avail->size(); j++)
                avail_dif.append(0);

            for (int j=0; j < min_avail->size(); j++)
            {
                /* if more resources are needed than are available, than more inital available resources
                 * have to exist
                 */
                if (data->at(i).need.at(j) > avail->at(j))
                {
                    avail_dif[j] = data->at(i).need.at(j) - avail->at(j);
                    (*needed_avail)[j] += avail_dif[j];
                    (*avail)[j] = data->at(i).need.at(j);
                }

                (*avail)[j] += data->at(i).allocation.at(j);
            }
            //found is increased, indicating that a process is added to the safe sequence
            found++;
            find_min(data, min_avail, needed_avail, avail, used, found);
            found--;
            used[i] = false;

            //remove the required resources by the latest tested sequence
            for (int j=0; j < min_avail->size(); j++)
            {
                (*needed_avail)[j] -= avail_dif.at(j);
                (*avail)[j] -= avail_dif.at(j);
                (*avail)[j] -= data->at(i).allocation.at(j);
            }

        }
    }
    //if a safe sequence is found
    if (found == data->size())
    {
        int min_val = 0, need_val = 0;
        //the sum of all resources types is calculated
        for (int i=0; i < min_avail->size(); i++)
        {
            min_val += min_avail->at(i);
            need_val += needed_avail->at(i);
        }

        //if a sequence with less required resources is found, it is saved
        if (min_val > need_val)
            for (int i=0; i < min_avail->size(); i++)
                if (min_avail->at(i) > needed_avail->at(i))
                    (*min_avail)[i] = needed_avail->at(i);
    }

}

void calc_min_available(QVector<banker_process_t> *data, QVector<int> *available)
{
    int resource_size = available->size();
    //reset the available resources to find the minimum
    available->clear();
    QVector<int> needed_avail, avail;

    //initialize vectors
    for (int i=0; i < resource_size; i++)
    {
        available->append(9999);
        needed_avail.append(0);
        avail.append(0);
    }

    const int used_size = data->size();
    bool *used = new bool[used_size];

    //initialize bool array
    for (int i=0; i < used_size; i++)
        used[i] = false;

    find_min(data, available, &needed_avail, &avail, used, 0);
    delete[] used;
}
