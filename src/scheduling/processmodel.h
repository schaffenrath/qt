/*
 * Author: Robert Schaffenrath
 * Supervisor: Dr. Thomas Fahringer
 *
 * University of Innsbruck, Institute of Computer Science, http://dps.uibk.ac.at
 */

#ifndef PROCESSMODEL_H
#define PROCESSMODEL_H

#include <QAbstractListModel>
#include <QVector>

class ProcessController;

class ProcessModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(ProcessController *list READ list WRITE setList)

public:

    enum RoleNames {
        IdRole = Qt::UserRole,
        InsertionRole = Qt::UserRole+2,
        DurationRole = Qt::UserRole+3,
        PriorityRole = Qt::UserRole+4,
        TicketRole = Qt::UserRole+5,
        QuantumUsageRole = Qt::UserRole+6,
    };

    Q_ENUMS(scheduleAlgorithms)
    enum scheduleAlgorithms {
        FCFS, SJF, SRTF, RR, PSnon, PSpre, LS, LS_theoretical
    };

    Q_ENUMS(metricType)
    enum metricType {
        RESPONSE_TIME, WAIT_TIME, TURNAROUND_TIME
    };



    explicit ProcessModel(QObject *parent = nullptr);

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    // Editable:
    bool setData(const QModelIndex &index, const QVariant &value,
                 int role = Qt::EditRole) override;

    Qt::ItemFlags flags(const QModelIndex& index) const override;

    virtual QHash<int, QByteArray> roleNames() const override;

    ProcessController *list() const;

    void setList(ProcessController *list);

private:
    QHash<int, QByteArray> proc_roleNames;
    ProcessController *procList;
};

#endif // PROCESSMODEL_H
