#include <iostream>
#include <vector>
#include <algorithm>
#include "process.h"
#include "generate_scheduling.h"

#include <QDebug>


/*
 *	This file contains the functions to count the conflicts for FCFS, SJF, SRTF, RR and PS. These functions are called in generate_processes.cpp
 *  to verify the randomly generated attributes of the processes.
 *
 *  The scheduling functions in this file differ from the functions in scheduling.cpp, because mainly the process list is here a vector, as where
 *  in scheduling.cpp it is a double linked list. The functions here also don't provide information on the order of execution and no additional data
 *  is stored.
 */


bool is_earlier(process_t x, process_t y)
{
    //if insertionTime is equal, the process with lower id is preferred
    if (x.arrival == y.arrival)
        return x.id < y.id;
    return x.arrival < y.arrival;
}

bool is_shorter(process_t x, process_t y)
{
    //if duration and insertion are equal, order via id
    if (x.duration == y.duration)
        return x.id < y.id;

    return x.duration < y.duration;
}

bool has_higher_priority(process_t x, process_t y)
{
    //if priority and insertion are equal, order via id
    if (x.priority == y.priority)
        return x.id < y.id;

    return x.priority < y.priority;
}

bool is_earlier_and_shorter(process_t x, process_t y)
{
    //if insertion and duration are equal, order via id
    if (x.arrival == y.arrival)
    {
        if (x.duration == y.duration)
        {
            return x.id < y.id;
        }
        else
        {
            return x.duration < y.duration;
        }
    }

    return x.arrival < y.arrival;
}

bool is_earlier_and_higher_priority(process_t x, process_t y)
{
    //if insertion and priority are equal, order via id
    if (x.arrival == y.arrival)
    {
        if (x.priority == y.priority)
        {
            return x.id < y.id;
        }
        else
        {
            return x.priority < y.priority;
        }
    }
    return x.arrival < y.arrival;
}

bool has_more_tickets(process_t x, process_t y)
{
    if (x.tickets == y.tickets)
        return x.id < y.id;

    return x.tickets < y.tickets;
}

void check_sjf(std::vector<process_t> *proc_list, generation_process_queue_t *queue, process_t *new_process,
               std::vector<unsigned short> *conflict_remaining)
{
    if (proc_list->empty())
    {
        process_t empty_proc;
        empty_proc.id = 99;
        empty_proc.duration = 0;
        queue->running_process = empty_proc;
        return;
    }

    std::sort(proc_list->begin(), proc_list->end(), is_earlier_and_shorter);

    if (queue->timestamp >= new_process->arrival)
        queue->process_queue.push_back(proc_list->back());


    else
    {
        bool added = false;
        while (queue->timestamp < new_process->arrival)
        {
            if (!added && proc_list->back().arrival >= queue->timestamp)
                queue->timestamp = proc_list->back().arrival;

            else if (queue->running_process.duration != 0)
                queue->timestamp += queue->running_process.duration;

            else if (!queue->process_queue.empty() && queue->process_queue.front().arrival < new_process->arrival)
                queue->timestamp = queue->process_queue.front().arrival;

            else if (new_process->arrival >= queue->timestamp)
                queue->timestamp = new_process->arrival;

            if (!added && queue->timestamp >= proc_list->back().arrival)
            {
                queue->process_queue.push_back(proc_list->back());
                std::sort(queue->process_queue.begin(), queue->process_queue.end(), is_shorter);
                added = true;
            }

            // if queue is empty, set running to null
            if (queue->process_queue.empty())
                queue->running_process.duration = 0;

            else if (new_process->arrival >= queue->timestamp && queue->running_process.duration == 0)
            {
                size_t i = 0;
                while (i < queue->process_queue.size() && queue->process_queue.at(i).arrival > queue->timestamp)
                    i++;
                if (i != queue->process_queue.size())
                {
                    queue->running_process = queue->process_queue.at(i);
                    queue->process_queue.erase(queue->process_queue.begin()+i);
                    conflict_remaining->push_back(queue->running_process.duration);
                }
                else
                    queue->running_process.duration = 0;
            }

            else
                queue->running_process.duration = 0;
        }
    }

    if (queue->timestamp == queue->running_process.arrival)
        conflict_remaining->push_back(queue->running_process.duration);

    for (process_t proc : queue->process_queue)
        conflict_remaining->push_back(proc.duration);



}


void check_srtf(std::vector<process_t> *proc_list, generation_process_queue_t *queue, process_t *new_process,
                std::vector<unsigned short> *conflict_remaining)
{
    if (proc_list->empty())
    {
        process_t empty_proc;
        empty_proc.remaining_time = 0;
        empty_proc.id = 0;
        queue->running_process = empty_proc;

        return;
    }

    bool added = false;
    while (queue->timestamp < new_process->arrival)
    {
        if ((added && queue->timestamp + queue->running_process.remaining_time >= new_process->arrival) ||
                (added && queue->process_queue.empty() && queue->running_process.remaining_time == 0))
            queue->timestamp = new_process->arrival;

        else if (queue->process_queue.empty() && queue->running_process.remaining_time == 0)
            queue->timestamp = proc_list->back().arrival;

        else if (queue->timestamp + queue->running_process.remaining_time < new_process->arrival)
            queue->timestamp += queue->running_process.remaining_time;

        if (!added && queue->timestamp >= proc_list->back().arrival)
        {
            queue->process_queue.push_back(proc_list->back());
            std::sort(queue->process_queue.begin(), queue->process_queue.end(), is_shorter);
            added = true;
        }

        if (queue->running_process.remaining_time != 0)
        {
            if (queue->running_process.remaining_time <= (queue->timestamp - queue->prev_timestamp))
                queue->running_process.remaining_time = 0;
            else
                queue->running_process.remaining_time -= (queue->timestamp - queue->prev_timestamp);
        }

        size_t i=0;
        while (i < queue->process_queue.size() && queue->process_queue.at(i).arrival > queue->timestamp)
            i++;
        if (i != queue->process_queue.size() && queue->process_queue.at(i).remaining_time < queue->running_process.remaining_time)
        {
            queue->running_process.arrival = queue->timestamp;
            queue->process_queue.push_back(queue->running_process);

            queue->running_process = queue->process_queue.at(i);
            queue->process_queue.erase(queue->process_queue.begin()+i);
        }

        else if (i != queue->process_queue.size() && queue->running_process.remaining_time == 0)
        {
            queue->running_process = queue->process_queue.at(i);
            queue->process_queue.erase(queue->process_queue.begin()+i);
        }

        queue->prev_timestamp = queue->timestamp;
    }

    conflict_remaining->push_back(queue->running_process.remaining_time);
    for (process_t proc : queue->process_queue)
        conflict_remaining->push_back(proc.remaining_time);
}



void check_rr(std::vector<process_t> *proc_list, generation_process_queue_t *queue, unsigned short quantum,
              std::vector<unsigned short> *conflict_arrival)
{
    unsigned short quantum_time = 0;

    if (proc_list->empty())
    {
        process_t empty_proc;
        empty_proc.remaining_time = 0;
        empty_proc.id = 0;
        queue->running_process = empty_proc;
        return;
    }

    else if (queue->timestamp == 0 && proc_list->back().arrival == 0)
    {
        queue->running_process = proc_list->back();
    }

    else
    {
        bool added = false;
        quantum_time = queue->timestamp+quantum;
        while (queue->timestamp < proc_list->back().arrival || !added)
        {
            if (!queue->process_queue.empty() && queue->running_process.remaining_time == 0)
            {
                queue->timestamp = queue->process_queue.front().arrival;
                quantum_time = queue->timestamp + quantum;
            }

            else if (queue->running_process.remaining_time != 0 && queue->timestamp + queue->running_process.remaining_time <= quantum_time)
            {
                queue->timestamp += queue->running_process.remaining_time;
                quantum_time = queue->timestamp + quantum;
            }

            else
            {
                queue->timestamp = quantum_time;
                quantum_time += quantum;
            }

            if (!added && queue->timestamp >= proc_list->back().arrival)
            {
                queue->process_queue.push_back(proc_list->back());
                added = true;
            }

            if (queue->running_process.remaining_time <= (queue->timestamp - queue->prev_timestamp))
                queue->running_process.remaining_time= 0;
            else
            {
                queue->running_process.remaining_time -= (queue->timestamp - queue->prev_timestamp);

                if (queue->timestamp == quantum_time - quantum)
                {
                    queue->running_process.arrival = queue->timestamp;
                    queue->process_queue.push_back(queue->running_process);
                }
            }

            if (!queue->process_queue.empty())
            {
                queue->running_process = queue->process_queue.front();
                queue->process_queue.erase(queue->process_queue.begin());
            }

            queue->prev_timestamp = queue->timestamp;
        }
    }

    conflict_arrival->push_back(queue->timestamp);

    //Calc afterwards
    generation_process_queue_t queue_copy = *queue;
    while (!queue_copy.process_queue.empty())
    {
        if (queue_copy.timestamp + queue_copy.running_process.remaining_time <= quantum_time)
        {
            queue_copy.timestamp += queue_copy.running_process.remaining_time;
            quantum_time = queue_copy.timestamp;
            if (!queue->process_queue.empty())
            {
                conflict_arrival->push_back(queue_copy.timestamp);
                queue_copy.running_process = queue_copy.process_queue.front();
                queue_copy.process_queue.erase(queue_copy.process_queue.begin());
            }
        }

        else
        {
            queue_copy.timestamp = quantum_time;
            quantum_time += quantum;
            conflict_arrival->push_back(queue_copy.timestamp);
            queue_copy.running_process.remaining_time -= (queue_copy.timestamp - queue_copy.prev_timestamp);
            queue_copy.process_queue.push_back(queue_copy.running_process);
            queue_copy.running_process = queue_copy.process_queue.front();
            queue_copy.process_queue.erase(queue_copy.process_queue.begin());
        }
        queue_copy.prev_timestamp = queue_copy.timestamp;

    }
}


void check_ps_non(std::vector<process_t> *proc_list, generation_process_queue_t *queue, process_t *new_process,
                  std::vector<unsigned short> *conflict_priority)
{
    if (proc_list->empty())
    {
        process_t empty_proc;
        empty_proc.duration = 0;
        queue->running_process = empty_proc;
        return;
    }

    std::sort(proc_list->begin(), proc_list->end(), is_earlier_and_higher_priority);

    if (queue->timestamp >= new_process->arrival)
        queue->process_queue.push_back(proc_list->back());

    else
    {
        bool added = false;
        while (queue->timestamp < new_process->arrival)
        {
            if (!added && proc_list->back().arrival >= queue->timestamp)
                queue->timestamp = proc_list->back().arrival;

            else if (queue->running_process.duration != 0)
                queue->timestamp += queue->running_process.duration;

            else if (!queue->process_queue.empty() && queue->process_queue.front().arrival < new_process->arrival)
                queue->timestamp = queue->process_queue.front().arrival;

            else if (new_process->arrival >= queue->timestamp)
                queue->timestamp = new_process->arrival;

            if (!added && queue->timestamp >= proc_list->back().arrival)
            {
                queue->process_queue.push_back(proc_list->back());
                std::sort(queue->process_queue.begin(), queue->process_queue.end(), has_higher_priority);
                added = true;
            }

            if (proc_list->empty() && queue->running_process.duration == 0)
                queue->timestamp = new_process->arrival;

            else if (queue->process_queue.empty())
                queue->running_process.duration = 0;

            else if (new_process->arrival >= queue->timestamp + queue->running_process.duration)
            {
                size_t i=0;
                while (i < queue->process_queue.size() && queue->process_queue.at(i).arrival > queue->timestamp)
                    i++;
                if (i != queue->process_queue.size())
                {
                    queue->running_process = queue->process_queue.at(i);
                    queue->process_queue.erase(queue->process_queue.begin()+i);
                }

                else
                    queue->running_process.duration = 0;
            }

            else
                queue->running_process.duration = 0;
        }
    }

    if (queue->timestamp == queue->running_process.arrival)
        conflict_priority->push_back(queue->running_process.priority);

    for (process_t proc : queue->process_queue)
        if (proc.arrival <= queue->timestamp)
            conflict_priority->push_back(proc.priority);
}



void check_ps_pre(std::vector<process_t> *proc_list, generation_process_queue_t *queue, process_t *new_process,
                  std::vector<unsigned short> *conflict_priority)
{
    if (proc_list->empty())
    {
        process_t empty_proc;
        empty_proc.duration = 0;
        queue->running_process = empty_proc;
        return;
    }

    bool added = false;
    while (queue->timestamp < new_process->arrival)
    {
        if ((added && queue->timestamp + queue->running_process.remaining_time >= new_process->arrival) ||
                (added && queue->process_queue.empty() && queue->running_process.remaining_time == 0))
            queue->timestamp = new_process->arrival;

        else if (queue->process_queue.empty() && queue->running_process.remaining_time == 0)
            queue->timestamp = proc_list->back().arrival;

        else if (queue->timestamp + queue->running_process.remaining_time < new_process->arrival)
            queue->timestamp += queue->running_process.remaining_time;

        if (!added && queue->timestamp >= proc_list->back().arrival)
        {
            queue->process_queue.push_back(proc_list->back());
            std::sort(queue->process_queue.begin(), queue->process_queue.end(), has_higher_priority);
            added = true;
        }

        if (queue->running_process.duration != 0)
        {
            if (queue->running_process.duration <= (queue->timestamp - queue->prev_timestamp))
                queue->running_process.duration = 0;
            else
                queue->running_process.duration -= (queue->timestamp - queue->prev_timestamp);
        }

        size_t i=0;
        while (i < queue->process_queue.size() && queue->process_queue.at(i).arrival > queue->timestamp)
            i++;

        if (i != queue->process_queue.size() && queue->process_queue.at(i).priority < queue->running_process.priority)
        {
            queue->running_process.arrival = queue->timestamp;
            queue->process_queue.push_back(queue->running_process);

            queue->running_process = queue->process_queue.at(i);
            queue->process_queue.erase(queue->process_queue.begin()+i);
        }

        else if (i != queue->process_queue.size() && queue->running_process.duration == 0)
        {
            queue->running_process = queue->process_queue.at(i);
            queue->process_queue.erase(queue->process_queue.begin()+i);
        }

        queue->prev_timestamp = queue->timestamp;
    }

    conflict_priority->push_back(queue->running_process.priority);

    for (process_t proc : queue->process_queue)
        conflict_priority->push_back(proc.priority);
}

void check_ls(std::vector<process_t> *proc_list, generation_process_queue_t *queue,
              std::vector<unsigned short> *conflict_ls, unsigned short quantum)
{
    unsigned short quantum_time = 0;

    if (proc_list->empty())
    {
        process_t empty_proc;
        empty_proc.duration = 0;
        queue->running_process = empty_proc;
        return;
    }

    else if (queue->timestamp == 0 && proc_list->back().arrival == 0)
    {
        queue->running_process = proc_list->back();
    }

    else
    {
        bool added = false;
        quantum_time = queue->timestamp+quantum;
        while (queue->timestamp < proc_list->back().arrival || !added)
        {
            if (!queue->process_queue.empty() && queue->running_process.remaining_time == 0)
            {
                queue->timestamp = queue->process_queue.front().arrival;
                quantum_time = queue->timestamp + quantum;
            }

            else if (queue->running_process.remaining_time != 0 && queue->running_process.quantum_usage < queue->running_process.remaining_time)
            {
                if (queue->running_process.quantum_usage == 0)
                {
                    for (int i=0; i < 100; i++)
                        conflict_ls->push_back(i);
                    return;
                }

                queue->timestamp += queue->running_process.quantum_usage;
                quantum_time = queue->timestamp + quantum;

                if (queue->running_process.compensation_tickets == 0)
                {
                    queue->running_process.compensation_tickets = (queue->running_process.tickets / ((double)queue->running_process.quantum_usage / quantum)) - queue->running_process.tickets;
                    queue->running_process.tickets += queue->running_process.compensation_tickets;
                }
            }

            else if (queue->running_process.remaining_time != 0 && queue->timestamp + queue->running_process.remaining_time <= quantum_time)
            {
                queue->timestamp += queue->running_process.remaining_time;
                quantum_time = queue->timestamp + quantum;
            }

            else
            {
                queue->timestamp = quantum_time;
                quantum_time += quantum;
            }

            if (!added && queue->timestamp >= proc_list->back().arrival)
            {
                queue->process_queue.push_back(proc_list->back());
                std::sort(queue->process_queue.begin(), queue->process_queue.end(), has_more_tickets);
                added = true;
            }

            if (queue->running_process.remaining_time <= (queue->timestamp - queue->prev_timestamp)) {
                queue->running_process.remaining_time= 0;
            }

            else
            {
                if ((int) queue->running_process.remaining_time - (int)(queue->timestamp - queue->prev_timestamp) < 0)
                    queue->running_process.remaining_time = 0;
                else
                    queue->running_process.remaining_time -= (queue->timestamp - queue->prev_timestamp);

                if (queue->timestamp == quantum_time - quantum)
                {
                    queue->running_process.arrival = queue->timestamp;
                    queue->process_queue.push_back(queue->running_process);
                    std::sort(queue->process_queue.begin(), queue->process_queue.end(), has_more_tickets);
                }
            }

            if (!queue->process_queue.empty())
            {

                queue->running_process = queue->process_queue.front();
                queue->process_queue.erase(queue->process_queue.begin());
            }
            queue->prev_timestamp = queue->timestamp;
        }
    }

    conflict_ls->push_back(queue->running_process.tickets);

    for (process_t procs : queue->process_queue)
        conflict_ls->push_back(procs.tickets);
}
