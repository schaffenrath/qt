#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <cmath>
#include <random>
#include <chrono>
#include "schedule_functions.h"

using namespace std;


/*
 *  This file contains the helper functions for the scheduling in scheduling.cpp. These functions handle the insertion of a new process,
 *  split a process if a preemtive algorithm was chosen, find specific processes...
 */

mt19937 gen2(chrono::system_clock::now().time_since_epoch().count());


unsigned short find_event_timestamp(process_list_t *proc_list, event_list_head_tail_t *event_list)
{
    /* the initail value is the arrival of the first process in the process queue
     * this is necessary for the case, that the event_list is empty, because than
     * the timestamp for the first event is the arrival of the first process
     */
    unsigned short new_position = proc_list->process->arrival;

    if (event_list->tail != NULL)
    {
        //if no process is running, the arrival time of the next process is used
        if (RUNNING == NULL && PTR != NULL)
            new_position = PTR->process->arrival;

        /* if the new event is a arrival of a process, the remaining time of the running
         * process is reduced
         */
        else if (PTR != NULL && PTR->process->arrival < POSITION + REMAINING)
        {
            new_position = PTR->process->arrival;
            REMAINING -= new_position - POSITION;
        }
        else
        {
            //if not all processes from the process list were in the process queue
            if (PTR != NULL)
            {
                new_position = PTR->process->arrival;
                //if the running process terminates before the arrival of a new process
                if ((int) REMAINING - ((int) new_position - (int) POSITION) <= 0)
                {
                    new_position = POSITION + REMAINING;
                    REMAINING = 0;
                }
                /* otherwise the time difference between the new position and the old one is
                 * substracted of the remaining time
                 */
                else
                     REMAINING -= new_position - POSITION;
            }
            /* if all processes from the process list were in the process queue, a new
             * event can only be a change of the running process
             */
            else
            {
                new_position = POSITION + REMAINING;
                REMAINING = 0;
            }
        }
    }
    return new_position;
}

/* this function is only called in the add_event_list_node function, if new processes arrive
 * in the process queue
 */
event_list_head_tail_t *add_equal_insertion_proc(process_list_t *proc_list, event_list_head_tail_t *event_list)
{
    /* this variable is used to calculate the sum of all tickets from each process
     * in the process queue
     */
    unsigned short new_total_tickets = 0;
    // if no processes exists,  nothing can be done
    if (proc_list == NULL && PTR == NULL)
        return NULL;

    process_list_t *new_process_queue = NULL;
    process_list_t *node_iterator = NULL;
    unsigned short current_position = 0;

    //if the event list is empty, the current position is the arrival of the first process
    if (event_list->tail == NULL)
    {
        current_position = proc_list->process->arrival;
        node_iterator = proc_list;
    }

    //if the process queue is empty
    else if (event_list->tail->process_queue == NULL)
    {
        //if all processes were already added to the process queue, everthing is done
        if (PTR == NULL)
            return NULL;

        /* the first argument for proc_copy is the head of the list, which should be copied
         * the second argument changes the pointer from a process of the old list to the same
         * process in the copied list
         */
        new_process_queue = proc_copy(event_list->tail->process_queue, &event_list->tail->process_queue);

        current_position = PTR->process->arrival;
        node_iterator = PTR;
        new_total_tickets = event_list->tail->total_tickets;
    }

    else
    {
        process_list_t *new_running_pointer = RUNNING;
        current_position = PTR->process->arrival;
        new_total_tickets = event_list->tail->total_tickets;

        //copy list of processes previously in the event_list
        new_process_queue = proc_copy(event_list->tail->process_queue, &new_running_pointer);

        if (PTR != NULL &&
                PTR->process->arrival <= current_position)
        {
            node_iterator = PTR;
            while (node_iterator != NULL && node_iterator->process->arrival <= current_position)
            {
                proc_append(&new_process_queue, node_iterator->process);
                new_total_tickets += node_iterator->process->tickets;
                node_iterator = node_iterator->next;
            }
        }

        //add new event to the event list
        event_list_append(&event_list, new_process_queue);

        //update PTR
        if (node_iterator != NULL)
            PTR = node_iterator;

        else
            PTR = NULL;

        //update pointers to new event
        event_list->tail->total_tickets = new_total_tickets;
        RUNNING = new_running_pointer;

        return event_list;
    }

    //add all processes earliest insertion time
    while (node_iterator != NULL && node_iterator->process->arrival == current_position)
    {
        proc_append(&new_process_queue, node_iterator->process);
        new_total_tickets += node_iterator->process->tickets;
        node_iterator = node_iterator->next;
    }

    event_list_append(&event_list, new_process_queue);
    event_list->tail->total_tickets = new_total_tickets;
    PTR = node_iterator;

    return event_list;
}

//this function is called in every scheduling algrotihm to add a new event
event_list_head_tail_t *add_event_list_node(process_list_t *proc_list, event_list_head_tail_t *event_list,
                            unsigned short new_position)
{
    /* add elements from process list if event_list empty or insertion of process
   * was before or right at current position */
    if (event_list->tail == NULL || event_list->tail->process_queue == NULL ||
            (PTR != NULL &&
             new_position >= PTR->process->arrival))

    {
        event_list = add_equal_insertion_proc(proc_list, event_list);

        if (event_list != NULL)
            POSITION = new_position;
    }

    /* if no new process arrive until the next time slice, the process queue is just copied
     * and pointers are updated
     */
    else
    {
        process_list_t *processes_in_event_list = NULL;
        process_list_t *running_ptr = RUNNING;
        processes_in_event_list = proc_copy(event_list->tail->process_queue, &running_ptr);
        event_list_append(&event_list, processes_in_event_list);
        PTR = PREV_PTR;
        RUNNING = running_ptr;
        event_list->tail->total_tickets = event_list->tail->prev->total_tickets;
        POSITION = new_position;
    }

    return event_list;
}


void interrupt_running_process(event_list_head_tail_t **event_list)
{
    event_list_head_tail_t *event_list_pointer = *event_list;
    process_t *split_process = event_list_pointer->tail->running_process->process;
    split_process->arrival = event_list_pointer->tail->timestamp;

    proc_remove_process(&event_list_pointer->tail->process_queue,
                        event_list_pointer->tail->running_process->process->id);

    if (split_process->remaining_time <= 0)
    {
        split_process->remaining_time = 0;
        event_list_pointer->tail->total_tickets -= split_process->tickets;
    }

    else
        proc_append(&event_list_pointer->tail->process_queue, split_process);

    *event_list = event_list_pointer;
}

process_list_t *find_shortest(process_list_t *process_list)
{
    if (process_list == NULL)
        return NULL;

    process_list_t *proc_iterator = process_list;
    process_list_t *shortest_process = process_list;

    while (proc_iterator != NULL)
    {
        if (proc_iterator->process->remaining_time == shortest_process->process->remaining_time)
        {
            if (proc_iterator->process->arrival < shortest_process->process->id)
                shortest_process = proc_iterator;

            else if(proc_iterator->process->arrival == shortest_process->process->arrival &&
                    proc_iterator->process->id < shortest_process->process->id)
                shortest_process = proc_iterator;
        }
        if (proc_iterator->process->remaining_time < shortest_process->process->remaining_time)
            shortest_process = proc_iterator;

        proc_iterator = proc_iterator->next;
    }

    return shortest_process;
}


process_list_t *find_highest_priority(process_list_t *process_list)
{
    if (process_list == NULL)
        return NULL;

    process_list_t *proc_iterator = process_list;
    process_list_t *highest_priority = process_list;

    while (proc_iterator != NULL)
    {
        if (proc_iterator->process->priority == highest_priority->process->priority)
        {
            if (proc_iterator->process->arrival < highest_priority->process->id)
                highest_priority = proc_iterator;

            else if(proc_iterator->process->arrival == highest_priority->process->arrival &&
                    proc_iterator->process->id < highest_priority->process->id)
                highest_priority = proc_iterator;
        }

        if (proc_iterator->process->priority < highest_priority->process->priority)
            highest_priority = proc_iterator;

        proc_iterator = proc_iterator->next;
    }
    return highest_priority;
}


process_list_t *find_lottery_winner(process_list_t *process_list, unsigned short total_tickets)
{
    if (process_list == NULL || total_tickets == 0)
        return NULL;

    uniform_int_distribution<int> rando(0, total_tickets+1);
    unsigned short winner;
    winner = (unsigned short) rando(gen2) % (total_tickets+1);

    process_list_t *ticket_iterator = process_list;
    unsigned short ticket_sum = 0;

    while (ticket_iterator != NULL && ticket_sum < winner)
    {
        ticket_sum += ticket_iterator->process->tickets;
        if (ticket_sum < winner)
            ticket_iterator = ticket_iterator->next;
    }

    return ticket_iterator;
}

process_list_t *find_different_lottery_winner(process_list_t *process_list, unsigned short total_tickets, unsigned short avoid_id)
{
    if (process_list == NULL || total_tickets == 0)
        return NULL;

    uniform_real_distribution<double> rando(0.0,(double) total_tickets+1);
    unsigned short winner;
    winner = (unsigned short) rando(gen2) % (total_tickets+1);

    process_list_t *ticket_iterator = process_list;
    unsigned short ticket_sum = 0;

    while (ticket_iterator != NULL && ticket_sum < winner)
    {
        ticket_sum += ticket_iterator->process->tickets;
        if (ticket_sum >= winner && ticket_iterator->process->id == avoid_id)
        {
            if (ticket_iterator->prev != NULL)
                ticket_iterator = ticket_iterator->prev;
            else if (ticket_iterator->next != NULL)
                ticket_iterator = ticket_iterator->next;
        }
        if (ticket_sum < winner)
            ticket_iterator = ticket_iterator->next;
    }

    return ticket_iterator;
}

process_list_t *find_most_tickets(process_list_t *process_list, unsigned short total_tickets)
{
    if (process_list == NULL || total_tickets == 0)
        return NULL;

    process_list_t *max_tickets = process_list;
    process_list_t *ticket_iterator = process_list;

    while (ticket_iterator != NULL)
    {
        if (ticket_iterator->process->tickets > max_tickets->process->tickets)
            max_tickets = ticket_iterator;

        else if (ticket_iterator->process->tickets == max_tickets->process->tickets)
        {
            if (ticket_iterator->process->arrival < max_tickets->process->arrival)
                max_tickets = ticket_iterator;

            else if (ticket_iterator->process->arrival == max_tickets->process->arrival &&
                     ticket_iterator->process->id < max_tickets->process->id)
                max_tickets = ticket_iterator;
        }
        ticket_iterator = ticket_iterator->next;
    }

    return max_tickets;
}

process_list_t *find_different_most_tickets(process_list_t *process_list, unsigned short total_tickets, unsigned short avoid_id)
{
    if (process_list == NULL || total_tickets == 0)
        return NULL;

    process_list_t *max_tickets = process_list;
    process_list_t *ticket_iterator = process_list;

    while (ticket_iterator != NULL)
    {
        if (ticket_iterator->process->tickets > max_tickets->process->tickets && ticket_iterator->process->id != avoid_id)
            max_tickets = ticket_iterator;

        else if (ticket_iterator->process->tickets == max_tickets->process->tickets)
        {
            if (ticket_iterator->process->arrival < max_tickets->process->arrival)
                max_tickets = ticket_iterator;

            else if (ticket_iterator->process->arrival == max_tickets->process->arrival &&
                     ticket_iterator->process->id < max_tickets->process->id)
                max_tickets = ticket_iterator;
        }

        ticket_iterator = ticket_iterator->next;
    }

    return max_tickets == NULL ? process_list : max_tickets;
}



void update_timestamp(event_list_head_tail_t **event_list)
{
    event_list_head_tail_t *event_list_handler = *event_list;
    if (event_list_handler->tail->prev != NULL)
        event_list_handler->tail->timestamp = (event_list_handler->tail->prev->timestamp +
                                         event_list_handler->tail->prev->running_process->process->remaining_time);
    else
        event_list_handler->tail->timestamp = event_list_handler->tail->running_process->process->arrival;
}

short check_tickets(process_list_t *proc_list_head)
{
    process_list_t *it = proc_list_head;
    while (it != NULL)
    {
        if (it->process->tickets != 0)
            it = it->next;
        else
            return 0;
    }
    return 1;
}

//unused function for LS when process with interruption is considered to use the hole quantum
short did_use_quantum(event_list_head_tail_t *event_list, unsigned short quantum)
{
    if (event_list->tail == NULL)
        return 0;

    event_list_t *it = event_list->tail;
    short counter = 0;
    while (counter < quantum-1 && it->prev != NULL)
    {
        if (it->prev->running_process == NULL || it->prev->running_process->process->id != event_list->tail->running_process->process->id)
            return 0;

        it = it->prev;
        counter++;
    }
    return 1;
}
