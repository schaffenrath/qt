/*
 * Author: Robert Schaffenrath
 * Supervisor: Dr. Thomas Fahringer
 *
 * University of Innsbruck, Institute of Computer Science, http://dps.uibk.ac.at
 */

#ifndef GENERATE_PROCESSES_H
#define GENERATE_PROCESSES_H

#include "process.h"

#include <vector>

std::vector<process_t> generate_procs(unsigned short amount, unsigned short max,
                                      unsigned short quantum, unsigned short lsQuantum, unsigned short alt_solutions);

#endif
