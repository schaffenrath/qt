/*
 * Author: Robert Schaffenrath
 * Supervisor: Dr. Thomas Fahringer
 *
 * University of Innsbruck, Institute of Computer Science, http://dps.uibk.ac.at
 */

#ifndef PROCESS_H
#define PROCESS_H

#include <cstdlib>

typedef struct process
{
    unsigned short id = 0;
    unsigned short arrival = 0;
    unsigned short duration = 0;
    unsigned short remaining_time = 0;
    unsigned short priority = 0;
    unsigned short tickets = 0;
    unsigned short compensation_tickets = 0;
    unsigned short quantum_usage = 0;
} process_t;

typedef struct process_list
{
    process_t *process;
    struct process_list *next = NULL;
    struct process_list *prev = NULL;
} process_list_t;

process_list_t *proc_find (process_list_t *head, const unsigned short proc_id);
void proc_insert (process_list_t **head, process_t *new_process);
void proc_append (process_list_t **head, process_t *new_process);
process_list_t * proc_copy (process_list_t *head, process_list_t **old_to_new_pointer);
void proc_remove_front (process_list_t **head);
void proc_remove_pointer (process_list_t **head, process_list_t *pointer_to_remove);
void proc_remove_process (process_list_t **head, const unsigned short process_id);
void proc_clear (process_list_t *head);

#endif
