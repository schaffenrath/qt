/*
 * Author: Robert Schaffenrath
 * Supervisor: Dr. Thomas Fahringer
 *
 * University of Innsbruck, Institute of Computer Science, http://dps.uibk.ac.at
 */

#ifndef QUEUE_H
#define QUEUE_H

#define ID event_list->tail->running_process->process->id
#define INSERTION event_list->tail->running_process->process->arrival
#define REMAINING event_list->tail->running_process->process->remaining_time
#define RUNNING event_list->tail->running_process
#define POSITION event_list->tail->timestamp
#define QUANTUM_USAGE event_list->tail->running_process->process->quantum_usage
#define PTR event_list->tail->proc_list_pointer
#define PTR_ARRIVAL event_list->tail->proc_list_pointer->process->arrival;
#define PREV_RUNNING event_list->tail->prev->running_process
#define PREV_PTR event_list->tail->prev->proc_list_pointer
#define PREV_POSITION event_list->tail->prev->timestamp
#define PREV_REMAINING event_list->tail->prev->running_process->process->remaining_time
#define LIST_PROC event_list->tail->process_queue->process

#include "process.h"

typedef struct event_list_t
{
    unsigned short timestamp = 0;
    unsigned short total_tickets = 0;
    process_list_t *running_process = NULL;
    process_list_t *proc_list_pointer = NULL;
    process_list_t *process_queue = NULL;
    struct event_list_t *next = NULL;
    struct event_list_t *prev = NULL;
} event_list_t;

typedef struct event_list_head_tail_t
{
    event_list_t *head = NULL;
    event_list_t *tail = NULL;
} event_list_head_tail_t;

void event_list_append (event_list_head_tail_t **event_list, process_list_t *process_list);
event_list_t *event_list_copy_last_node (event_list_t *event_list);
void event_list_clear (event_list_head_tail_t *event_list);

#endif
