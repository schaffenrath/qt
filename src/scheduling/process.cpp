#include <cstdio>
#include <cstdlib>
#include "process.h"

#include <QDebug>

using namespace std;


/*
 * This file contains all functions to handle the double linked list containing processes.
 */

process_list_t *proc_find(process_list_t *proc_list, const unsigned short proc_id)
{
    process_list_t *proc_iterator = proc_list;
    while (proc_iterator != NULL && proc_iterator->process->id != proc_id)
        proc_iterator = proc_iterator->next;
    return proc_iterator;
}


// the insert function is only used when user creates new processes, to sort the proc list
void proc_insert(process_list_t **head, process_t *new_process)
{
    process_list_t *new_node = (process_list_t*) malloc(sizeof(process_list_t));
    new_node->process = new_process;
    new_node->next = NULL;
    new_node->prev = NULL;

    if (*head == NULL)
    {
        *head = new_node;
        return;
    }

    process_list_t *node_iterator = *head;
    // iterate to insertion spot depending on insertion time
    while (node_iterator->next != NULL &&
           node_iterator->process->arrival < new_process->arrival)
        node_iterator = node_iterator->next;

    // if insertion time equal, insertion after id
    if (node_iterator->process->arrival == new_process->arrival)
    {
        while (node_iterator->next != NULL &&
               node_iterator->process->arrival == new_process->arrival &&
               node_iterator->process->id < new_process->id){

            node_iterator = node_iterator->next;
        }

        if (node_iterator->next == NULL &&
            node_iterator->process->arrival <= new_process->arrival &&
            node_iterator->process->id < new_process->id)
        {
            node_iterator->next = new_node;
            new_node->prev = node_iterator;
            return;
        }
    }

    // append when last node
    else if (node_iterator->next == NULL)
    {
        if (node_iterator->process->arrival < new_process->arrival)
        {
            node_iterator->next = new_node;
            new_node->prev = node_iterator;
            return;
        }
    }

    // new head when first node
    if (node_iterator->prev == NULL)
    {
        new_node->next = node_iterator;
        node_iterator->prev = new_node;
        *head = new_node;
        return;
    }

    new_node->prev = node_iterator->prev;
    new_node->next = node_iterator;
    node_iterator->prev->next = new_node;
    node_iterator->prev = new_node;
}


void proc_append(process_list_t **head, process_t *new_process)
{
    if (new_process == NULL)
        return;

    process_list_t *new_node = (process_list_t*) malloc(sizeof(process_list_t));
    new_node->process = new_process;
    new_node->next = NULL;
    new_node->prev = NULL;

    if (*head == NULL)
        *head = new_node;
    else
    {
        process_list_t *node_iterator = *head;

        while (node_iterator->next != NULL)
            node_iterator = node_iterator->next;
        node_iterator->next = new_node;
        new_node->prev = node_iterator;
    }
}


// the second argument changes the pointer from a proc of an old list to the same proc of the copied list
process_list_t *proc_copy(process_list_t *head, process_list_t **old_to_new_pointer)
{
    if (head == NULL)
        return NULL;

    process_list_t *new_list_head = (process_list_t*) malloc(sizeof(process_list_t));
    new_list_head->process = head->process;
    new_list_head->prev = NULL;
    new_list_head->next = NULL;

    if (old_to_new_pointer != NULL && *old_to_new_pointer == head)
        *old_to_new_pointer = new_list_head;

    process_list_t *prev_node = new_list_head;
    process_list_t *node_iterator = head->next;

    while (node_iterator != NULL)
    {
        process_list_t *new_list_node = (process_list_t*) malloc(sizeof(process_list_t));
        new_list_node->process = node_iterator->process;
        new_list_node->prev = prev_node;
        new_list_node->next = NULL;
        prev_node->next = new_list_node;

        if (old_to_new_pointer != NULL && *old_to_new_pointer == node_iterator)
            *old_to_new_pointer = new_list_node;

        node_iterator = node_iterator->next;
        prev_node = new_list_node;
    }

    return new_list_head;
}


void proc_remove_front(process_list_t **head)
{
    if (*head == NULL)
        return;

    process_list_t *new_head = *head;
    if (new_head->next == NULL)
        free(new_head);

    else
    {
        new_head = new_head->next;
        free(*head);
        *head = new_head;
    }
}


void proc_remove_pointer(process_list_t **head, process_list_t *pointer_to_remove)
{
    process_list_t *new_head = *head;
    if (new_head == pointer_to_remove)
    {
        if (new_head->next != NULL)
        {
            new_head = new_head->next;
            free(new_head->prev);
            new_head->prev = NULL;
            *head = new_head;
        }

        else
        {
            free(*head);
            *head = NULL;
        }
    }

    else if (pointer_to_remove->next == NULL)
    {
        pointer_to_remove = pointer_to_remove->prev;
        free(pointer_to_remove->next);
        pointer_to_remove->next = NULL;
    }

    else
    {
        process_list_t *tmp_save = pointer_to_remove;
        pointer_to_remove->prev->next = pointer_to_remove->next;
        pointer_to_remove->next->prev = pointer_to_remove->prev;
        free(tmp_save);
    }
}


void proc_remove_process(process_list_t **head, const unsigned short process_id)
{
    if (*head == NULL)
        return;

    process_list_t *new_head = *head;

    if (process_id == new_head->process->id)
    {
        if (new_head->next == NULL){
            free(*head);
            *head = NULL;
        }

        else
        {
            new_head = new_head->next;
            free(new_head->prev);
            new_head->prev = NULL;
            *head = new_head;
        }
    }

    else
    {
        process_list_t *remove_iterator = *head;
        while (remove_iterator != NULL && process_id != remove_iterator->process->id)
            remove_iterator = remove_iterator->next;

        if (remove_iterator != NULL)
        {
            if (remove_iterator->next == NULL)
            {
                remove_iterator->prev->next = NULL;
                free(remove_iterator);
            }

            else
            {
                process_list_t *tmp_save = remove_iterator;
                remove_iterator->prev->next = remove_iterator->next;
                remove_iterator->next->prev = remove_iterator->prev;
                free(tmp_save);
            }
        }
    }
}


void proc_clear(process_list_t *head)
{
    if (head == NULL)
        return;

    if (head->next == NULL)
    {
        free(head);
        return;
    }

    process_list_t *node_iterator = head->next;
    while (node_iterator->next != NULL)
    {
        free(node_iterator->prev);
        node_iterator = node_iterator->next;
    }
    free(node_iterator->prev);
    free(node_iterator);
}    
