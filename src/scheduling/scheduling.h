/*
 * Author: Robert Schaffenrath
 * Supervisor: Dr. Thomas Fahringer
 *
 * University of Innsbruck, Institute of Computer Science, http://dps.uibk.ac.at
 */

#ifndef SCHEDULING_H
#define SCHEDULING_H

#include "schedule_functions.h"
#include "process.h"
#include "event_list.h"

enum ScheduleAlgorithms
{
    FCFS, SJF, SRTF, RR, PSnon, PSpre, LS, LS_theoretical
};

enum PreemptionType
{
    NON_PREEMPTIVE, PREEMPTIVE
};

event_list_head_tail_t *scheduling (short scheduling_type, process_list_t *proc_list_head);
event_list_head_tail_t *scheduling (short scheduling_type, process_list_t *proc_list_head, unsigned short additional_argument, bool forceInterruption);

#endif
