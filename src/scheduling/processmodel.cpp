#include "processmodel.h"

#include "processcontroller.h"

#include <QtCore>

ProcessModel::ProcessModel(QObject *parent)
    : QAbstractListModel(parent)
    , procList(nullptr)
{
    proc_roleNames[IdRole] = "id";
    proc_roleNames[InsertionRole] = "insertion";
    proc_roleNames[DurationRole] = "duration";
    proc_roleNames[PriorityRole] = "priority";
    proc_roleNames[TicketRole] = "tickets";
    proc_roleNames[QuantumUsageRole] = "quantumUsage";
}

int ProcessModel::rowCount(const QModelIndex &parent) const
{
    // For list models only the root node (an invalid parent) should return the list's size. For all
    // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
    if (parent.isValid() || !procList)
        return 0;

    return procList->getProcList().size();
}

QVariant ProcessModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid() || !procList)
        return QVariant();

    const process_t proc = procList->getProcList().at(index.row());

    switch (role) {
    case IdRole:
        return QVariant(proc.id);
    case InsertionRole:
        return QVariant(proc.arrival);
    case DurationRole:
        return QVariant(proc.duration);
    case PriorityRole:
        return QVariant(proc.priority);
    case TicketRole:
        return QVariant(proc.tickets);
    case QuantumUsageRole:
        return QVariant(proc.quantum_usage);
    }
    return QVariant();
}

bool ProcessModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (!procList)
        return false;

    process_t proc = procList->getProcList().at(index.row());
    switch (role) {
    case IdRole:
        proc.id = value.toInt();
        break;
    case InsertionRole:
        proc.arrival = value.toInt();
        break;
    case DurationRole:
        proc.duration = value.toInt();
        break;
    case PriorityRole:
        proc.priority= value.toInt();
        break;
    case TicketRole:
        proc.tickets = value.toInt();
        break;
    case QuantumUsageRole:
        proc.quantum_usage = value.toInt();
    }
    if (procList->setProcAt(index.row(), proc))
    {
        emit dataChanged(index, index, QVector<int>() << role);
        return true;
    }
    return false;
}

Qt::ItemFlags ProcessModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::NoItemFlags;

    return Qt::ItemIsEditable; // FIXME: Implement me!
}

QHash<int, QByteArray> ProcessModel::roleNames() const
{
    return proc_roleNames;
}


ProcessController *ProcessModel::list() const
{
    return procList;
}

void ProcessModel::setList(ProcessController *list)
{
    beginResetModel();

    if (procList)
        procList->disconnect(this);

    procList = list;

    if (procList)
    {
        connect(procList, &ProcessController::preProcAppend, this, [=]() {
            const int index = procList->getProcList().size();
            beginInsertRows(QModelIndex(), index, index);
        });

        connect(procList, &ProcessController::postProcAppend, this, [=]() {
            endInsertRows();
        });

        connect(procList, &ProcessController::preProcRemove, this, [=](int index) {
            beginRemoveRows(QModelIndex(), index, index);
        });

        connect(procList, &ProcessController::postProcRemove, this, [=]() {
            endRemoveRows();
        });

        connect(procList, &ProcessController::preClearList, this, [=]() {
            beginRemoveRows(QModelIndex(), 0, procList->getProcList().size());
        });


        connect(procList, &ProcessController::postClearList, this, [=]() {
            endRemoveRows();
        });

    }

    endResetModel();
}
