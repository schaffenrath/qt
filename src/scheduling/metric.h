/*
 * Author: Robert Schaffenrath
 * Supervisor: Dr. Thomas Fahringer
 *
 * University of Innsbruck, Institute of Computer Science, http://dps.uibk.ac.at
 */

#ifndef METRICS_H
#define METRICS_H

#include "event_list.h"
#include "process.h"

enum MetricType
{
    RESPONSE_TIME, WAIT_TIME, TURNAROUND_TIME
};

typedef struct proc_metric
{
    unsigned short id;
    float turnaround_time;
    float response_time;
    short response_flag;
    float wait_time;
    proc_metric *next;
} proc_metric_t;

typedef struct metric
{
    proc_metric_t *proc_metric_ptr;
    float avg_turnaround_time;
    float avg_response_time;
    float avg_wait_time;
} metric_t;

metric_t calc_metrics(event_list_head_tail_t *queue);
void clear_metrics(proc_metric_t *metric_ptr);

#endif
