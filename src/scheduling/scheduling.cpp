#include <cstdlib>
#include <climits>
#include "scheduling.h"
#include "metric.h"


#include <QDebug>

using namespace std;


/*
 *  This file contains all scheduling algorithms.
 *
 */

event_list_head_tail_t *fcfs_next (process_list_t *proc_list, event_list_head_tail_t *event_list)
{
    //new_position is the time, when a new event happens
    unsigned short new_position = find_event_timestamp(proc_list, event_list);

    event_list = add_event_list_node (proc_list, event_list, new_position);

    //if the add_event_list_node function returned a valid value
    if (event_list != NULL)
    {
        //if the running process is terminated
        if (RUNNING != NULL && REMAINING == 0)
        {
            //the running process is removed from the process queu
            proc_remove_process (&event_list->tail->process_queue, ID);
            RUNNING = NULL;

            //returning NULL signlizes that the scheduling process is done
            if (event_list->tail->process_queue == NULL && PTR == NULL)
                return NULL;
        }
        //if no process is running, the process in front of the process queue is selected
        if (RUNNING == NULL)
            RUNNING = event_list->tail->process_queue;
    }

    return event_list;
}

event_list_head_tail_t *sjf_next (process_list_t *proc_list, event_list_head_tail_t *event_list)
{
    unsigned short new_position = find_event_timestamp(proc_list, event_list);

    event_list = add_event_list_node (proc_list, event_list, new_position);

    if (event_list != NULL)
    {
        // if the running process is terminated, remove it
        if (RUNNING != NULL && REMAINING == 0)
        {
            proc_remove_process (&event_list->tail->process_queue, ID);
            RUNNING = NULL;

            if (event_list->tail->process_queue == NULL && PTR == NULL)
                return NULL;
        }

        //if no process is running, select shortest one
        if (RUNNING == NULL)
           RUNNING = find_shortest (event_list->tail->process_queue);
    }

    return event_list;
}

event_list_head_tail_t *srtf_next (process_list_t *proc_list, event_list_head_tail_t *event_list)
{
    unsigned short new_position = find_event_timestamp(proc_list, event_list);

    event_list = add_event_list_node (proc_list, event_list, new_position);


    /* if no valid value is returned by the add_event_list_node function, return NULL which
     * which indicates, that the scheduling process is done
     */
    if (event_list == NULL)
        return NULL;

    if (RUNNING != NULL && REMAINING == 0)
    {
        proc_remove_process (&event_list->tail->process_queue, ID);

        RUNNING = PREV_RUNNING;

        if (event_list->tail->process_queue == NULL && PTR == NULL)
            return NULL;
    }

    // for first execution and if no process is running
    if (event_list->tail->prev == NULL || RUNNING == NULL)
    {
        RUNNING = find_shortest (event_list->tail->process_queue);
        POSITION = new_position;
        return event_list;
    }

    // if all initial processes were added to event_list
    if (PTR == NULL && PREV_PTR == NULL)
    {
        //calculate the remaining time of the running process
        int time_remaining = (int) PREV_REMAINING - ((int) new_position - (int) POSITION);

        if (time_remaining >= 0)
            PREV_REMAINING = (unsigned short) time_remaining;

        else
            PREV_REMAINING = 0;

        RUNNING = find_shortest (event_list->tail->process_queue);
        POSITION = new_position;

        return event_list;
    }

    POSITION = new_position;

    /* safe the remaining time of the latest running process to check if shorter process
     * has arrived
     */
    unsigned short prev_running_remaining_time =
            PREV_REMAINING - (new_position - event_list->tail->prev->timestamp);

    process_list_t *shortest_process = find_shortest (event_list->tail->process_queue);

    // if running process is done before next one starts
    if ((POSITION + REMAINING) <= PREV_PTR->process->arrival)
    {
        proc_remove_process (&event_list->tail->process_queue, PREV_RUNNING->process->id);

        shortest_process = find_shortest (event_list->tail->process_queue);
        POSITION = new_position;

    } // if shorter process is in event_list
    else if (prev_running_remaining_time > shortest_process->process->remaining_time)
    {
        POSITION = new_position;
        //reinserts running process in process queue with updated remaining time
        interrupt_running_process (&event_list);

        shortest_process = find_shortest (event_list->tail->process_queue);
    }
    else
    {
        //if latest running process is still the shortest process, it keeps running
        POSITION = new_position;
        int time_remaining = (int) REMAINING -
                ((int) new_position - (int) event_list->tail->prev->timestamp);


        if (time_remaining < 0)
        {
            REMAINING = 0;
            proc_remove_process (&event_list->tail->process_queue, PREV_RUNNING->process->id);

            shortest_process = find_shortest (event_list->tail->process_queue);
        }
    }

    RUNNING = shortest_process;
    return event_list;
}

event_list_head_tail_t *rr_next(process_list_t *proc_list, event_list_head_tail_t *event_list, unsigned short quantum, bool forceInterruption __attribute__((unused)))
{
    //this variable helps to determine if the event is only a process arrival
    unsigned short new_proc_inserted = 0;
    int new_position = 0;
    //quantum_time saves the time slice when a quantum is over, therefore static
    static unsigned short quantum_time = 0;
    
    /* because the quantum has to considered for finding the timestamp of the event,
     * the find_event_timestamp function can not be applied
     */
    if (event_list->tail == NULL)
    {
        new_position = proc_list->process->arrival;
        new_proc_inserted = 1;
    }

    //if no process is running but still more processes have to be added to the process queue
    else if (RUNNING == NULL && PTR != NULL)
    {
        new_position = PTR_ARRIVAL;
        quantum_time = new_position + quantum;
    }
    
    //if running process terminates before quantum is over
    else if (POSITION + REMAINING < quantum_time)
    {
        new_position = POSITION + REMAINING;
    }

    //update the quantum time
    else
    {
        if (POSITION == quantum_time)
            new_position = quantum_time + quantum;

        else
            new_position = quantum_time;
    }

    //update the quantum time
    quantum_time = new_position;
    
    //if new process arrives before quantum is over of running process terminates
    if (event_list->tail != NULL && PTR != NULL && (int)PTR->process->arrival <= new_position)
    {
        new_position = PTR_ARRIVAL;
        new_proc_inserted = 1;
    }
    
    event_list = add_event_list_node(proc_list, event_list, new_position);
    
    //if first execution
    if (event_list->tail->prev == NULL || RUNNING == NULL)
    {
        RUNNING = event_list->tail->process_queue;
        POSITION = new_position;
        quantum_time = new_position + quantum;
        return event_list;
    }

    //if event only covers arrival of new processes
    if (new_proc_inserted && new_position != quantum_time)
    {
        RUNNING = PREV_RUNNING;
        REMAINING -= (new_position - PREV_POSITION);
        POSITION = new_position;

        return event_list;
    }

    //if running process terminates
    if (POSITION + REMAINING == new_position)
    {
        proc_remove_process(&event_list->tail->process_queue, PREV_RUNNING->process->id);
        RUNNING = event_list->tail->process_queue;
        POSITION = new_position;

        quantum_time = new_position + quantum;

        //if a new process is selected, the counting of the quantum resets
        if (RUNNING == NULL && PTR == NULL)
            return NULL;

        return event_list;
    }
    
    //if running process gets interrupted, because of quantum
    else
    {
        POSITION = new_position;

        //calculate remaining time
        if ((int)REMAINING - (new_position - (int)PREV_POSITION) <= 0)
            REMAINING = 0;
        else
            REMAINING -= (new_position - (int)PREV_POSITION);

        //appends the running process to the process queue again
        interrupt_running_process(&event_list);

        RUNNING = event_list->tail->process_queue;

        if (RUNNING == NULL && PTR == NULL)
            return NULL;

        quantum_time = new_position + quantum;
    }

    return event_list;
}

event_list_head_tail_t *ps_next (process_list_t *proc_list, event_list_head_tail_t *event_list, PreemptionType preemption_type)
{
    unsigned short new_position = find_event_timestamp(proc_list, event_list);

    event_list = add_event_list_node (proc_list, event_list, new_position);

    if (event_list == NULL)
        return NULL;

    if (event_list->tail->process_queue == NULL && PTR == NULL)
        return NULL;

    if (RUNNING != NULL && REMAINING == 0)
    {
        proc_remove_process (&event_list->tail->process_queue, ID);

        if (event_list->tail->process_queue == NULL && PTR == NULL)
            return NULL;
    }

    // for first execution
    if (event_list->tail->prev == NULL || RUNNING == NULL)
    {
        RUNNING = find_highest_priority (event_list->tail->process_queue);
        POSITION = INSERTION;
        return event_list;
    }

    // if all initial processes were added to event_list
    if (PTR == NULL && PREV_PTR == NULL)
    {
        POSITION = new_position;
        RUNNING = find_highest_priority (event_list->tail->process_queue);
        return event_list;
    }

    //new_position =  PREV_PTR->process.arrival;
    POSITION = new_position;

    process_list_t *priority_process = NULL;

    /* if the algorithm is used with preemption, always the process with the highest
     * priority gets selected
     */
    if (preemption_type == PREEMPTIVE)
        priority_process = find_highest_priority (event_list->tail->process_queue);

    /* if the non-preemptive version is run, the running process has to terminate
     * before another process can get selected
     */
    else
        priority_process = RUNNING;

    // if running process is done before next one starts
    if ((POSITION + PREV_REMAINING) <= PREV_PTR->process->arrival)
    {
        POSITION = new_position;
        proc_remove_process (&event_list->tail->process_queue, PREV_RUNNING->process->id);

        priority_process = find_highest_priority (event_list->tail->process_queue);
    }
    // if process with higher priority was found
    else if (preemption_type == PREEMPTIVE &&
             PREV_RUNNING->process->priority > priority_process->process->priority)
    {
        POSITION = new_position;
        interrupt_running_process (&event_list);

        priority_process = find_highest_priority (event_list->tail->process_queue);
    }

    else
    {
        POSITION = new_position;
        if (REMAINING <= 0)
        {
            REMAINING = 0;
            proc_remove_process (&event_list->tail->process_queue, PREV_RUNNING->process->id);

            priority_process = find_highest_priority (event_list->tail->process_queue);
        }
    }

    RUNNING = priority_process;
    return event_list;
}

event_list_head_tail_t *ps_non_next(process_list_t *proc_list, event_list_head_tail_t *event_list)
{
    return ps_next(proc_list, event_list, NON_PREEMPTIVE);
}

event_list_head_tail_t *ps_pre_next(process_list_t *proc_list, event_list_head_tail_t *event_list)
{
    return ps_next(proc_list, event_list, PREEMPTIVE);
}

event_list_head_tail_t *ls_next (process_list_t *proc_list, event_list_head_tail_t *event_list, unsigned short quantum, bool forceInterruption)
{
    short new_proc_inserted = 0;
    int new_position = 0;
    //used to keep track of the expired time of a quantum
    static unsigned short quantum_time = 0;

    //if it's the first execution
    if (event_list->tail == NULL)
    {
        new_position = proc_list->process->arrival;
        new_proc_inserted = 1;
        quantum_time = 0;
    }

    //if no process is running but not all processes were added to the process queue so far
    else if (RUNNING == NULL && PTR != NULL)
    {
        new_position = PTR_ARRIVAL;
        quantum_time = new_position + quantum;
    }

    //if the running process terminates before the quantum is over
    else if (RUNNING != NULL && POSITION + REMAINING < quantum_time && ((REMAINING <= QUANTUM_USAGE && QUANTUM_USAGE != 0) || QUANTUM_USAGE == 0))
    {
        new_position = POSITION + REMAINING;
        quantum_time = new_position + quantum;
    }

    // if quantum usage is specified for the running process
    else if (RUNNING != NULL && QUANTUM_USAGE != 0 && QUANTUM_USAGE != quantum)
    {
        new_position = POSITION + QUANTUM_USAGE;
    }

    else
    {
        new_position = quantum_time;
    }
    
    //if a new process arrives in the process queue before another event happens
    if (event_list->tail != NULL && PTR != NULL && (int)PTR->process->arrival <= new_position)
    {
        new_position = PTR_ARRIVAL;
        new_proc_inserted = 1;
    }

    //if no process is running and all processes were added to the process queue
    if (event_list->tail != NULL && RUNNING == NULL && PTR == NULL)
        return NULL;

    //helper variable needed for correction of POSITION
    unsigned short pos_before_add = 0;
    if (event_list->tail != NULL)
        pos_before_add = POSITION;

    //if a new process should be added call the function, otherwise just copy the last event
    if (new_proc_inserted)
        event_list = add_event_list_node(proc_list, event_list, new_position);

    else
        event_list->tail = event_list_copy_last_node(event_list->tail);

    //if no valid value was returned by the previously called function
    if (event_list == NULL)
        return NULL;

    //if its the first execution or no process is running
    if (event_list->tail->prev == NULL || RUNNING == NULL)
    {
        //find process which holds most tickets in the process queue
        RUNNING = find_lottery_winner (event_list->tail->process_queue, event_list->tail->total_tickets);

        if (RUNNING != NULL)
        {
            // if running process had compensation_tickets, they will be removed
            if (RUNNING->process->compensation_tickets > 0)
            {
                RUNNING->process->tickets -= RUNNING->process->compensation_tickets;
                event_list->tail->total_tickets -= RUNNING->process->compensation_tickets;
                RUNNING->process->compensation_tickets = 0;
            }

            POSITION = RUNNING->process->arrival;
            quantum_time = POSITION + quantum;
        }

        return event_list;
    }

    //if the running process terminates before the next quantum is over
    if (POSITION + REMAINING <= new_position && (QUANTUM_USAGE >= REMAINING || QUANTUM_USAGE == 0))
    {
        POSITION += REMAINING;
        REMAINING = 0;

        //before removing a process from the list, its tickets have to get removed from the sum
        event_list->tail->total_tickets -= PREV_RUNNING->process->tickets;
        proc_remove_process (&event_list->tail->process_queue, ID);

        RUNNING = find_lottery_winner (event_list->tail->process_queue, event_list->tail->total_tickets);

        if (RUNNING != NULL && RUNNING->process->compensation_tickets > 0)
        {
            RUNNING->process->tickets -= RUNNING->process->compensation_tickets;
            event_list->tail->total_tickets -= RUNNING->process->compensation_tickets;
            RUNNING->process->compensation_tickets = 0;
        }

        quantum_time = POSITION + quantum;
        return event_list;
    }

    //if process terminates within its quantum usage
    if (QUANTUM_USAGE > REMAINING && REMAINING < new_position - POSITION)
    {
        if (pos_before_add == POSITION)
            POSITION += REMAINING;

        event_list->tail->total_tickets -= PREV_RUNNING->process->tickets;
        proc_remove_process (&event_list->tail->process_queue, ID);

        RUNNING = find_lottery_winner (event_list->tail->process_queue, event_list->tail->total_tickets);

        if (RUNNING != NULL && RUNNING->process->compensation_tickets > 0)
        {
            RUNNING->process->tickets -= RUNNING->process->compensation_tickets;
            event_list->tail->total_tickets -= RUNNING->process->compensation_tickets;
            RUNNING->process->compensation_tickets = 0;
        }

        quantum_time = POSITION + quantum;
        return event_list;
    }

    //if the running process gets interrupted, due to quantum usage
    if (!(new_proc_inserted && POSITION - PREV_POSITION < QUANTUM_USAGE) && QUANTUM_USAGE != 0 && QUANTUM_USAGE != quantum && QUANTUM_USAGE < quantum_time - POSITION)
    {
        unsigned short compensation_tickets = 0;
        process_list_t * interrupted_process = NULL;

        //when a new proc gets inserted, the position will be updated automaticly to the new arrival
        if (pos_before_add == POSITION)
            POSITION += QUANTUM_USAGE;

        if ((int)REMAINING - (int)QUANTUM_USAGE < 0)
            REMAINING = 0;
        else
            REMAINING -= QUANTUM_USAGE;

        unsigned short saved_id = ID;
        interrupt_running_process (&event_list);
        RUNNING = proc_find(event_list->tail->process_queue, saved_id);
        interrupted_process = RUNNING;

        if (interrupted_process != NULL)
        {
            /* calculation of compensation tickets: 1/f * tickets where f is the used fraction
             * of the defined quantum
             */
            compensation_tickets = (unsigned short)
                    (interrupted_process->process->tickets / (((double) interrupted_process->process->quantum_usage / quantum))) - interrupted_process->process->tickets;

            /* compensation tickets are only assigned if other processes can be selected too
             * than otherwise the would not change anything
             * and if compensation tickets were not assigned already
             */
            if (forceInterruption && event_list->tail->process_queue != NULL && event_list->tail->process_queue->next != NULL && interrupted_process->process->compensation_tickets == 0)
            {
                event_list->tail->total_tickets += compensation_tickets;
                interrupted_process->process->tickets += compensation_tickets;
                interrupted_process->process->compensation_tickets = compensation_tickets;
            }

            /* if the force insterruption option was selected, which does not allow the
             * re-selection of the running process
             */
            if (forceInterruption)
                RUNNING = find_different_lottery_winner (event_list->tail->process_queue, event_list->tail->total_tickets, ID);

            else
                RUNNING = find_lottery_winner (event_list->tail->process_queue, event_list->tail->total_tickets);
        }

        else
            RUNNING = find_lottery_winner (event_list->tail->process_queue, event_list->tail->total_tickets);


        if (RUNNING != NULL && RUNNING->process->compensation_tickets > 0)
        {
            RUNNING->process->tickets -= RUNNING->process->compensation_tickets;
            event_list->tail->total_tickets -= RUNNING->process->compensation_tickets;
            RUNNING->process->compensation_tickets = 0;
        }

        quantum_time = POSITION + quantum;
        return event_list;
    }

    //if event is only arrival of new processes
    if (new_proc_inserted && (new_position != quantum_time && ((QUANTUM_USAGE == 0 || QUANTUM_USAGE == quantum) || (PREV_POSITION + QUANTUM_USAGE < new_position))))
    {
        RUNNING = PREV_RUNNING;
        REMAINING -= (new_position - PREV_POSITION);
        POSITION = new_position;
        return event_list;
    }

    else
    {
        unsigned short compensation_tickets = 0;
        process_list_t * interrupted_process = NULL;

        POSITION = new_position;

        if ((int)REMAINING - (int)(POSITION - PREV_POSITION) < 0 )
            REMAINING = 0;

        else
            REMAINING -= (POSITION - PREV_POSITION);

        interrupt_running_process (&event_list);
        RUNNING = PREV_RUNNING;

        //if the running process is interrupted due to the quantum usage
        if (QUANTUM_USAGE != 0 && QUANTUM_USAGE < quantum)
        {
            interrupted_process = proc_find(event_list->tail->process_queue, ID);

            //if the interrupted process did not terminate
            if (interrupted_process != NULL)
            {
                compensation_tickets = (unsigned short)
                        (interrupted_process->process->tickets / (((double) interrupted_process->process->quantum_usage / quantum))) - interrupted_process->process->tickets;

                if (event_list->tail->process_queue != NULL && event_list->tail->process_queue->next != NULL && interrupted_process->process->compensation_tickets == 0)
                {
                    event_list->tail->total_tickets += compensation_tickets;
                    interrupted_process->process->compensation_tickets = compensation_tickets;
                    interrupted_process->process->tickets += compensation_tickets;
                }

                if (forceInterruption)
                    RUNNING = find_different_lottery_winner (event_list->tail->process_queue, event_list->tail->total_tickets, ID);

                else
                    RUNNING = find_lottery_winner (event_list->tail->process_queue, event_list->tail->total_tickets);
            }
            else
                RUNNING = find_lottery_winner (event_list->tail->process_queue, event_list->tail->total_tickets);
        }

        else
            RUNNING = find_lottery_winner (event_list->tail->process_queue, event_list->tail->total_tickets);

        if (RUNNING->process->compensation_tickets > 0)
        {
            RUNNING->process->tickets -= compensation_tickets;
            event_list->tail->total_tickets -= compensation_tickets;
            RUNNING->process->compensation_tickets = 0;
        }

        quantum_time = POSITION + quantum;
        return event_list;
    }
    return NULL;
}

//the procedure of this function is similar to the ls_next funciton, which contains many comments
event_list_head_tail_t *ls_theoretical_next (process_list_t *proc_list, event_list_head_tail_t *event_list, unsigned short quantum, bool forceInterruption)
{
    short new_proc_inserted = 0;
    int new_position = 0;
    static unsigned short quantum_time = 0;

    if (event_list->tail == NULL)
    {
        new_position = proc_list->process->arrival;
        new_proc_inserted = 1;
        quantum_time = 0;
    }

    else if (RUNNING == NULL && PTR != NULL)
    {
        new_position = PTR_ARRIVAL;
        quantum_time = new_position + quantum;
    }

    else if (RUNNING != NULL && POSITION + REMAINING < quantum_time && ((REMAINING <= QUANTUM_USAGE && QUANTUM_USAGE != 0) || QUANTUM_USAGE == 0))
    {
        new_position = POSITION + REMAINING;
        quantum_time = new_position + quantum;
    }

    else if (RUNNING != NULL && QUANTUM_USAGE != 0 && QUANTUM_USAGE != quantum)
    {
        new_position = POSITION + QUANTUM_USAGE;
    }

    else
    {
        new_position = quantum_time;
    }

    if (event_list->tail != NULL && PTR != NULL && (int)PTR->process->arrival <= new_position)
    {
        new_position = PTR_ARRIVAL;
        new_proc_inserted = 1;
    }

    if (event_list->tail != NULL && RUNNING == NULL && PTR == NULL)
        return NULL;

    unsigned short pos_before_add = 0;
    if (event_list->tail != NULL)
        pos_before_add = POSITION;

    if (new_proc_inserted)
        event_list = add_event_list_node(proc_list, event_list, new_position);

    else
        event_list->tail = event_list_copy_last_node(event_list->tail);

    if (event_list == NULL)
        return NULL;

    if (event_list->tail->prev == NULL || RUNNING == NULL)
    {
        RUNNING = find_most_tickets (event_list->tail->process_queue, event_list->tail->total_tickets);

        if (RUNNING != NULL)
        {

            if (RUNNING->process->compensation_tickets > 0)
            {
                RUNNING->process->tickets -= RUNNING->process->compensation_tickets;
                event_list->tail->total_tickets -= RUNNING->process->compensation_tickets;
                RUNNING->process->compensation_tickets = 0;
            }

            POSITION = RUNNING->process->arrival;
            quantum_time = POSITION + quantum;
        }

        return event_list;
    }

    if (POSITION + REMAINING <= new_position && (QUANTUM_USAGE >= REMAINING || QUANTUM_USAGE == 0))
    {
        POSITION += REMAINING;
        REMAINING = 0;
        event_list->tail->total_tickets -= PREV_RUNNING->process->tickets;
        proc_remove_process (&event_list->tail->process_queue, ID);

        RUNNING = find_most_tickets (event_list->tail->process_queue, event_list->tail->total_tickets);

        if (RUNNING != NULL && RUNNING->process->compensation_tickets > 0)
        {
            RUNNING->process->tickets -= RUNNING->process->compensation_tickets;
            event_list->tail->total_tickets -= RUNNING->process->compensation_tickets;
            RUNNING->process->compensation_tickets = 0;
        }

        quantum_time = POSITION + quantum;
        return event_list;
    }

    if (QUANTUM_USAGE > REMAINING && REMAINING < new_position - POSITION)
    {
        if (pos_before_add == POSITION)
            POSITION += REMAINING;
        event_list->tail->total_tickets -= PREV_RUNNING->process->tickets;
        proc_remove_process (&event_list->tail->process_queue, ID);

        RUNNING = find_most_tickets(event_list->tail->process_queue, event_list->tail->total_tickets);

        if (RUNNING != NULL && RUNNING->process->compensation_tickets > 0)
        {
            RUNNING->process->tickets -= RUNNING->process->compensation_tickets;
            event_list->tail->total_tickets -= RUNNING->process->compensation_tickets;
            RUNNING->process->compensation_tickets = 0;
        }

        quantum_time = POSITION + quantum;
        return event_list;
    }

    if (!(new_proc_inserted && POSITION - PREV_POSITION < QUANTUM_USAGE) && QUANTUM_USAGE != 0 && QUANTUM_USAGE != quantum && QUANTUM_USAGE < quantum_time - POSITION)
    {
        unsigned short compensation_tickets = 0;
        process_list_t *interrupted_process = NULL;

        if (pos_before_add == POSITION)
            POSITION += QUANTUM_USAGE;

        if ((int)REMAINING - (int)QUANTUM_USAGE < 0)
            REMAINING = 0;
        else
            REMAINING -= QUANTUM_USAGE;

        unsigned short saved_id = ID;
        interrupt_running_process (&event_list);
        RUNNING = proc_find(event_list->tail->process_queue, saved_id);
        interrupted_process = RUNNING;

        if (interrupted_process != NULL)
        {
            compensation_tickets = (unsigned short)
                    (interrupted_process->process->tickets / (((double) interrupted_process->process->quantum_usage / quantum))) - interrupted_process->process->tickets;

            if (event_list->tail->process_queue != NULL && event_list->tail->process_queue->next != NULL && interrupted_process->process->compensation_tickets == 0)
            {
                event_list->tail->total_tickets += compensation_tickets;
                interrupted_process->process->tickets += compensation_tickets;
                interrupted_process->process->compensation_tickets = compensation_tickets;
            }

            if (forceInterruption)
                RUNNING = find_different_most_tickets (event_list->tail->process_queue, event_list->tail->total_tickets, interrupted_process->process->id);

            else
                RUNNING = find_most_tickets (event_list->tail->process_queue, event_list->tail->total_tickets);
        }

        else
            RUNNING = find_most_tickets(event_list->tail->process_queue, event_list->tail->total_tickets);

        if (RUNNING->process->compensation_tickets > 0)
        {
            RUNNING->process->tickets -= RUNNING->process->compensation_tickets;
            event_list->tail->total_tickets -= RUNNING->process->compensation_tickets;
            RUNNING->process->compensation_tickets = 0;
        }

        quantum_time = POSITION + quantum;
        return event_list;
    }

    if (new_proc_inserted && (new_position != quantum_time && ((QUANTUM_USAGE == 0 || QUANTUM_USAGE == quantum) || (PREV_POSITION + QUANTUM_USAGE < new_position))))
    {
        RUNNING = PREV_RUNNING;
        REMAINING -= (new_position - PREV_POSITION);
        POSITION = new_position;
        return event_list;
    }

    else
    {
        unsigned short compensation_tickets = 0;
        process_list_t * interrupted_process = NULL;

        POSITION = new_position;

        if ((int)REMAINING - (int)(POSITION - PREV_POSITION) < 0)
            REMAINING = 0;

        else
            REMAINING -= (POSITION - PREV_POSITION);

        interrupt_running_process (&event_list);
        RUNNING = PREV_RUNNING;

        if (QUANTUM_USAGE != 0 && QUANTUM_USAGE < quantum)
        {
            interrupted_process = proc_find(event_list->tail->process_queue, ID);

            if (interrupted_process != NULL)
            {
                compensation_tickets = (unsigned short)
                        (interrupted_process->process->tickets / (((double) interrupted_process->process->quantum_usage / quantum))) - interrupted_process->process->tickets;

                if (event_list->tail->process_queue != NULL && event_list->tail->process_queue->next != NULL && interrupted_process->process->compensation_tickets == 0)
                {
                    event_list->tail->total_tickets += compensation_tickets;
                    interrupted_process->process->compensation_tickets = compensation_tickets;
                    interrupted_process->process->tickets += compensation_tickets;
                }

                if (forceInterruption)
                    RUNNING = find_different_most_tickets (event_list->tail->process_queue, event_list->tail->total_tickets, interrupted_process->process->id);

                else
                    RUNNING = find_most_tickets (event_list->tail->process_queue, event_list->tail->total_tickets);
            }

            else
                RUNNING = find_most_tickets (event_list->tail->process_queue, event_list->tail->total_tickets);
        }

        else
            RUNNING = find_most_tickets (event_list->tail->process_queue, event_list->tail->total_tickets);


        if (RUNNING->process->compensation_tickets > 0)
        {
            RUNNING->process->tickets -= RUNNING->process->compensation_tickets;
            event_list->tail->total_tickets -= RUNNING->process->compensation_tickets;
            RUNNING->process->compensation_tickets = 0;
        }

        quantum_time = POSITION + quantum;

        return event_list;
    }
    return NULL;
}

event_list_head_tail_t *scheduling (short scheduling_type, process_list_t *proc_list_head)
{
    event_list_head_tail_t *check_result = NULL;
    event_list_head_tail_t * (*func_ptr)(process_list_t *, event_list_head_tail_t *);

    switch (scheduling_type)
    {
    case FCFS:
        func_ptr = &fcfs_next;
        break;

    case SJF:
        func_ptr = &sjf_next;
        break;

    case SRTF:
        func_ptr = &srtf_next;
        break;

    case PSnon:
        func_ptr = &ps_non_next;
        break;

    case PSpre:
        func_ptr = &ps_pre_next;
        break;

    default:
        return NULL;
    }

    event_list_head_tail_t *proc_event_list = (event_list_head_tail_t*) malloc (sizeof (event_list_head_tail_t));
    proc_event_list->head = NULL;
    proc_event_list->tail = NULL;

    do
    {
        check_result = (*func_ptr)(proc_list_head, proc_event_list);
        if (check_result != NULL)
            proc_event_list = check_result;

    }
    while (check_result != NULL);

    return proc_event_list;
}

event_list_head_tail_t *scheduling (short scheduling_type, process_list_t *proc_list_head, unsigned short additional_argument, bool forceInterruption)
{
    event_list_head_tail_t *check_result = NULL;
    event_list_head_tail_t * (*func_ptr)(process_list_t *, event_list_head_tail_t *, unsigned short, bool);

    switch (scheduling_type)
    {
    case RR:
        func_ptr = &rr_next;
        break;

    case LS:
        if (!check_tickets(proc_list_head))
            return NULL;
        func_ptr = &ls_next;
        break;

    case LS_theoretical:
        if (!check_tickets(proc_list_head))
            return NULL;

        func_ptr = &ls_theoretical_next;
        break;
    default:
        return NULL;
    }

    event_list_head_tail_t *proc_event_list = (event_list_head_tail_t*) malloc (sizeof (event_list_head_tail_t));
    proc_event_list->head = NULL;
    proc_event_list->tail = NULL;

    do
    {
        check_result = (*func_ptr)(proc_list_head, proc_event_list, additional_argument, forceInterruption);
        if (check_result != NULL)
            proc_event_list = check_result;

    }
    while (check_result != NULL);
    return proc_event_list;
}
