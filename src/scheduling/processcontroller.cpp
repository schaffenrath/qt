#include "processcontroller.h"
#include <vector>
#include <algorithm>
#include <QtCore>

int maxProcId = 0;

void convert_metrics(metric_t metricList, proc_result_t **resultItem);
void convertAndCalc(int scheduleType, process_list_t *input_proc_list, proc_result_t *new_results);
void convertAndCalc(int scheduleType, process_list_t *input_proc_list, proc_result_t *new_results, int quantum, bool forceInterruption);

ProcessController::ProcessController(QObject *parent) : QObject(parent)
{
    //test data
        process_t procs[8];
        procs[0].id = 0;
        procs[0].arrival =      0;
        procs[0].duration =     0;
        procs[0].priority =     0;
        procs[0].tickets =      0;
        procs[0].quantum_usage = 0;

//        procs[1].id =       1;
//        procs[1].arrival =  2;
//        procs[1].duration = 4;
//        procs[1].priority = 8;
//        procs[1].tickets =  8;
//        procs[1].quantum_usage = 2;


//        procs[2].id =       2;
//        procs[2].arrival =  3;
//        procs[2].duration = 1;
//        procs[2].priority = 5;
//        procs[2].tickets =  20;
//        procs[2].quantum_usage = 0;

//        procs[3].id =       3;
//        procs[3].arrival =  5;
//        procs[3].duration = 3;
//        procs[3].priority = 5;
//        procs[3].tickets = 30;
//        procs[3].quantum_usage = 2;

//        procs[4].id =       4;
//        procs[4].arrival =  10;
//        procs[4].duration = 3;
//        procs[4].priority = 5;
//        procs[4].tickets = 10;
//        procs[4].quantum_usage = 0;

//        procs[5].id =       5;
//        procs[5].arrival =  11;
//        procs[5].duration = 5;
//        procs[5].priority = 2;
//        procs[5].tickets = 15;
//        procs[5].quantum_usage = 0;

//        procs[6].id =       6;
//        procs[6].arrival =  15;
//        procs[6].duration = 8;
//        procs[6].priority = 5;
//        procs[6].tickets = 25;
//        procs[6].quantum_usage = 0;


        maxProcId+=1;

        procList.append(procs[0]);
//        procList.append(procs[1]);
//        procList.append(procs[2]);
//        procList.append(procs[3]);
//        procList.append(procs[4]);
//        procList.append(procs[5]);
//        procList.append(procs[6]);

    // process preview colors
    colorPalett.append("#881798");
    colorPalett.append("#10893E");
    colorPalett.append("#c62828");
    colorPalett.append("#0063B1");
    colorPalett.append("#FF6F00");
    colorPalett.append("#6D4C41");
    colorPalett.append("#69797E");
    colorPalett.append("#37474F");
    colorPalett.append("#6A1B9A");
    colorPalett.append("#9E9D24");
    colorPalett.append("#00838F");
    colorPalett.append("#AD1457");
    colorPalett.append("#283593");
}

QVector<process_t> ProcessController::getProcList() const
{
    return procList;
}

QVector<proc_result_t> ProcessController::getProcResultList() const
{
    return procResultList;
}

bool ProcessController::setProcAt(int index, const process_t &proc)
{
    if (index < 0 || index >= procList.size())
        return false;

    emit dataChanged();
    procList[index] = proc;
    return true;
}

void ProcessController::changeButtonState(int algorithm)
{
    if (algorithm >= 0 && algorithm < SCHEDULE_NUM)
    {
        selectedAlgorithms[algorithm] = !selectedAlgorithms[algorithm];
    }
}

bool ProcessController::getButtonState(int algorithm)
{
    if (algorithm >= 0 && algorithm < SCHEDULE_NUM)
        return selectedAlgorithms[algorithm];

    return false;
}

int ProcessController::getMaxTime()
{
    int maxValue = 0;
    for (process_t proc : procList)
    {
        if (proc.arrival + proc.duration > maxValue)
            maxValue = proc.arrival + proc.duration;
    }
    return maxValue + 1;
}

int ProcessController::getProcAmountAtQueue()
{
    return procList.size();
}

int ProcessController::getProcIdFromQueueAt(int index)
{
    return procList.at(index).id;
}

int ProcessController::getProcAmountAtPos(int index)
{
    int procCounter = 0;
    for (process_t proc : procList)
    {
        if (proc.arrival <= index && index < proc.arrival + proc.duration)
            procCounter++;
    }
    return procCounter;
}

int ProcessController::getProcId(int positionIndex, int itemIndex)
{
    int skipCounter = itemIndex;
    for (process_t proc: procList)
    {
        if (proc.arrival <= positionIndex && positionIndex < proc.arrival + proc.duration)
        {
            if (skipCounter == 0)
                return proc.id;
            skipCounter--;
        }
    }
    return 0;
}

QString ProcessController::getColorById(int index)
{
    if (index == -1)
        return QString("#ffffff");

    if (colorMap.contains(index))
        return colorMap[index];

    return colorPalett[index%colorPalett.size()];
}

void ProcessController::assignColor(int index, QString color)
{
    if (!colorMap.contains(index))
        colorMap.insert(index, color);
    else
        colorMap[index] = color;
}

int ProcessController::getRRQuantum()
{
    return rrQuantum;
}

void ProcessController::setRRQuantum(int quantum)
{
    rrQuantum = quantum;
}

void ProcessController::setLSQuantum(int quantum)
{
    lsQuantum = quantum;
}

int ProcessController::getLSQuantum()
{
    return lsQuantum;
}

void resetProcList(QVector<process_t> *procList, QVector<process_t> *calcList)
{
    for (int i=0; i < procList->size(); i++)
    {
        (*calcList)[i].id = procList->at(i).id;
        (*calcList)[i].arrival = procList->at(i).arrival;
        (*calcList)[i].duration = procList->at(i).duration;
        (*calcList)[i].remaining_time = procList->at(i).duration;
        (*calcList)[i].priority = procList->at(i).priority;
        (*calcList)[i].tickets = procList->at(i).tickets;
        (*calcList)[i].compensation_tickets = procList->at(i).compensation_tickets;
        (*calcList)[i].quantum_usage = procList->at(i).quantum_usage;
    }
}

void ProcessController::calcScheduling()
{
    if (procList.empty())
        return;

    for (int i=0; i < procList.size(); i++)
        calcList.push_back(procList[i]);

    process_list_t *input_proc_list = nullptr;

    //convert vector to linked list
    for (process_t &proc: calcList)
    {
        proc.remaining_time = proc.duration;
        proc_insert(&input_proc_list, &proc);
    }

    procResultList.clear();
    proc_result_t new_results;

    for (int i=0; i < SCHEDULE_NUM; i++)
    {
        new_results.eventList.clear();
        new_results.resultList.clear();
        if (selectedAlgorithms[i])
        {
            switch (i)
            {
            case FCFS:
                convertAndCalc(FCFS, input_proc_list, &new_results);
                procResultList.append(new_results);
                break;
            case SJF:
                convertAndCalc(SJF, input_proc_list, &new_results);
                procResultList.append(new_results);
                break;

            case SRTF:
                convertAndCalc(SRTF, input_proc_list, &new_results);
                procResultList.append(new_results);
                break;
            case RR:
                if (rrQuantum == 0)
                    break;
                convertAndCalc(RR, input_proc_list, &new_results, rrQuantum, false);
                procResultList.append(new_results);
                break;
            case PSnon:
                convertAndCalc(PSnon, input_proc_list, &new_results);
                procResultList.append(new_results);
                break;
            case PSpre:
                convertAndCalc(PSpre, input_proc_list, &new_results);
                procResultList.append(new_results);
                break;
            case LS:
                if (lsQuantum == 0)
                    break;
                convertAndCalc(LS, input_proc_list, &new_results, lsQuantum, forceInterruption);
                procResultList.append(new_results);
                break;
            case LS_theoretical:
                if (lsQuantum == 0)
                    break;
                convertAndCalc(LS_theoretical, input_proc_list, &new_results, lsQuantum, forceInterruption);
                procResultList.append(new_results);
                break;


            }
            resetProcList(&procList, &calcList);
        }
    }
    calcList.clear();
}

int ProcessController::getNumScheduleResults()
{
    if (procResultList.empty())
        return 0;

    int scheduleCounter = 0;
    for (int i=0; i < SCHEDULE_NUM; i++)
        if (selectedAlgorithms[i])
            scheduleCounter++;
    return scheduleCounter;
}

QString ProcessController::getScheduleType(int index)
{
    if (index >= procResultList.size())
        return "Error";

    switch(procResultList[index].scheduleAlgorithm) {
    case FCFS:
        return QString("First-come First-served");
    case SJF:
        return QString("Shortest Job First");
    case SRTF:
        return QString("Shortest Remaining Time First");
    case RR:
        return QString("Round Robin");
    case PSnon:
        return QString("Priority Scheduling Non-Preemptive");
    case PSpre:
        return QString("Priority Scheduling Preemptive");
    case LS:
        return QString("Lottery Scheduling");
    case LS_theoretical:
        return QString("Lottery Scheduling Theoretical");
    }

    return QString("Invalid Schedule Type");
}

bool ProcessController::isLS(int index)
{
    if (procResultList[index].scheduleAlgorithm == LS || procResultList[index].scheduleAlgorithm == LS_theoretical)
        return true;
    return false;
}

bool ProcessController::isLST(int index)
{
    if (procResultList[index].scheduleAlgorithm == LS_theoretical)
        return true;
    return false;
}

bool ProcessController::isForceInterruption()
{
    return forceInterruption;
}

void ProcessController::setFoceInterruption(bool state)
{
    forceInterruption = state;
}

int ProcessController::getMaxSchedulePos(int index)
{
    if (index >= procResultList.size())
        return 0;

    return procResultList.at(index).resultList.back().timestamp + 1;
}


int ProcessController::getProcAmountAtResultPos(int scheduleIndex, int posIndex)
{
    if (scheduleIndex >= procResultList.size())
        return 0;

    const proc_result_t *listPtr = &procResultList.at(scheduleIndex);

    for (int i=0; i < listPtr->eventList.size() - 1; i++)
    {
        if (listPtr->eventList.at(i).timestamp <= posIndex && posIndex < listPtr->eventList.at(i+1).timestamp &&
                listPtr->eventList.at(i).existsRunningProc) {
            return 1;
        }
    }
    return 0;
}

int ProcessController::getProcIdAtResultPos(int scheduleIndex, int posIndex)
{
    if (scheduleIndex >= procResultList.size())
        return 0;

    const proc_result_t *listPtr = &procResultList.at(scheduleIndex);

    for (int i=0; i < listPtr->eventList.size() - 1; i++)
    {
        if (listPtr->eventList.at(i).timestamp <= posIndex && posIndex < listPtr->eventList.at(i+1).timestamp)
            return listPtr->eventList.at(i).runningProc->id;
    }
    return 0;
}

void ProcessController::clearProcResultList()
{
    procResultList.clear();
}

void ProcessController::clearProcList()
{
    emit preClearList();
    procList.clear();
    emit postClearList();
    emit dataChanged();
    maxProcId = 0;
    emit preProcAppend();
    process_t proc;
    proc.id = maxProcId;
    proc.arrival = 0;
    proc.duration = 0;
    proc.priority = 0;
    proc.tickets = 0;
    proc.quantum_usage = 0;
    procList.append(proc);
    maxProcId++;
    emit postProcAppend();
    emit dataChanged();


}

bool ProcessController::isSchedulingValid()
{
    if (selectedAlgorithms[3] && rrQuantum == 0)
        return false;

    if (selectedAlgorithms[6] && lsQuantum == 0)
        return false;

    if (selectedAlgorithms[6])
    {
        for (process_t proc : procList)
            if (proc.tickets == 0 || proc.quantum_usage > lsQuantum || proc.quantum_usage == 0)
                return false;
    }

    return !procResultList.empty();
}

QString ProcessController::getSchedulingErrorMessage()
{
    for (int i=0; i < SCHEDULE_NUM; i++)
    {
        if (selectedAlgorithms[i] != false)
            break;

        if (i == SCHEDULE_NUM - 1)
            return QString("No Scheduling-Algorithm was selected!");
    }

    if (procList.empty())
        return QString("No processes to schedule. Please create or generate some!");

    if ((selectedAlgorithms[3] && rrQuantum == 0) || (selectedAlgorithms[6] && lsQuantum == 0))
        return QString("'0' is not a valid quantum. Please enter a value greater than that!");

    if (selectedAlgorithms[6])
    {
        for (process_t proc : procList)
        {
            if (proc.tickets == 0)
                return (QString("Process ") + QString::number(proc.id) +
                        QString(" has 0 tickets. Every process must have at least 1 ticket for Lottery Scheduling!"));

            if (proc.quantum_usage == 0)
                return (QString("Process ") + QString::number(proc.id) +
                        QString(" has LS Quantum Usage 0. This has to be at least 1 for Lottery Scheduling!"));

            if (proc.quantum_usage > lsQuantum)
                return (QString("Process ") + QString::number(proc.id) +
                        QString(" has a higher quantum usage than the quantum actually is!"));

        }
    }


    return QString("Error is not defined!");
}

void ProcessController::setClickPosition(float pos)
{
    clickPosition = pos;
}

float ProcessController::getClickPosition()
{
    return clickPosition;
}

bool sort_id_func(process_t proc_a, process_t proc_b)
{
    return (proc_a.id < proc_b.id);
}

void ProcessController::generateRandomList()
{
    if (generateAttributes[0] == 0 ||
            generateAttributes[0] > generateAttributes[1] ||
            generateAttributes[1] < generateAttributes[2] ||
            generateAttributes[0] < generateAttributes[3])
        return;

    emit preClearList();
    procList.clear();
    maxProcId = 0;
    emit postClearList();

    std::vector<process_t> generatedList = generate_procs(generateAttributes[0],generateAttributes[1],generateAttributes[2], generateAttributes[3], generateAttributes[4]);
    std::sort(generatedList.begin(), generatedList.end(), sort_id_func);
    for (process_t proc : generatedList)
    {
        emit preProcAppend();
        procList.append(proc);
        emit postProcAppend();
        if (proc.id >= maxProcId)
            maxProcId = proc.id+1;
    }
    emit dataChanged();
}

QString ProcessController::getAvgMetric(int metricType, int scheduleIndex)
{
    if (scheduleIndex >= procResultList.size())
        return QString("Out of range");
    switch(metricType)
    {
    case RESPONSE_TIME:
        return QString::number(procResultList.at(scheduleIndex).avg_response_time,'f',2);
    case WAIT_TIME:
        return QString::number(procResultList.at(scheduleIndex).avg_wait_time,'f',2);
    case TURNAROUND_TIME:
        return QString::number(procResultList.at(scheduleIndex).avg_turnaround_time,'f',2);
    }
    return QString("Wrong type");
}

void ProcessController::setMetricAttributes(int scheduleIndex, int id, int pos)
{
    metricScheduleIdx = scheduleIndex;
    metricId = id;
    metricPos = pos;
}

int ProcessController::getMetricId()
{
    return metricId;
}

int ProcessController::getMetricPos()
{
    return metricPos;
}

int ProcessController::getMetricResponse()
{
    if (metricScheduleIdx == -1 || metricId == -1 || metricPos == -1)
        return -1;

    for (proc_metric_t metric : procResultList.at(metricScheduleIdx).procMetric)
    {
        if (metric.id == metricId)
            return metric.response_time;
    }
    return -1;
}

int ProcessController::getMetricWait()
{
    if (metricScheduleIdx == -1 || metricId == -1 || metricPos == -1)
        return -1;

    for (proc_metric_t metric : procResultList.at(metricScheduleIdx).procMetric)
    {
        if (metric.id == metricId)
            return metric.wait_time;
    }
    return -1;
}

int ProcessController::getMetricTurnaround()
{
    if (metricScheduleIdx == -1 || metricId == -1 || metricPos == -1)
        return -1;

    for (proc_metric_t metric : procResultList.at(metricScheduleIdx).procMetric)
    {
        if (metric.id == metricId)
            return metric.turnaround_time;
    }
    return -1;
}

int ProcessController::getMetricAt(int index)
{
    if (metricScheduleIdx == -1 || metricId == -1 || metricPos == -1)
        return -1;

    const proc_result_t *listPtr = &procResultList.at(metricScheduleIdx);

    for (int i=0; i < listPtr->eventList.size() - 1; i++)
    {
        if (listPtr->eventList.at(i).timestamp <= metricPos && listPtr->eventList.at(i+1).timestamp > metricPos)
            if (index < listPtr->eventList.at(i).event_list.size())
                return listPtr->eventList.at(i).event_list.at(index)->id;
    }

    for (event_gap_list_t event : procResultList.at(metricScheduleIdx).eventList)
    {
        if (event.timestamp == metricPos)
        {
            if (index < event.event_list.size())
                return event.event_list.at(index)->id;
        }
    }
    return -1;
}

int ProcessController::getNumQueue()
{
    if (metricScheduleIdx == -1 || metricPos == -1 || metricId == -1)
        return 0;

    const proc_result_t *listPtr = &procResultList.at(metricScheduleIdx);

    for (int i=0; i < listPtr->eventList.size() - 1; i++)
    {
        if (listPtr->eventList.at(i).timestamp <= metricPos && listPtr->eventList.at(i+1).timestamp > metricPos)
            return listPtr->eventList.at(i).event_list.size();
    }
    return 0;
}

int ProcessController::getGenerateAmount()
{
    return generateAttributes[0];
}

int ProcessController::getGenerateMax()
{
    return generateAttributes[1];
}

int ProcessController::getGenerateRRQuantum()
{
    return generateAttributes[2];
}

int ProcessController::getGenerateLSQuantum()
{
    return generateAttributes[3];
}

int ProcessController::getGenerateAlternatives()
{
    return generateAttributes[4];
}

bool ProcessController::getGenerateIncludePS()
{
    return generateIncludePS;
}

void ProcessController::setGenerateAmount(int amount)
{
    generateAttributes[0] = amount;
}

void ProcessController::setGenerateMax(int max)
{
    generateAttributes[1] = max;
}

void ProcessController::setGenerateRRQuantum(int quantum)
{
    generateAttributes[2] = quantum;
}

void ProcessController::setGenerateLSQuantum(int quantum)
{
    generateAttributes[3] = quantum;
}

void ProcessController::setGenerateAlternatives(int alternatives)
{
    generateAttributes[4] = alternatives;
}

void ProcessController::setGenerateIncludePS(bool status)
{
    generateIncludePS = status;
}

bool ProcessController::isGenerateAttributesValid()
{
    if (generateAttributes[0] <= 0 ||
            generateAttributes[0] > generateAttributes[1] || generateAttributes[2] == 0 ||
            (generateAttributes[1] < generateAttributes[2] && generateAttributes[4] > 0)||
            (generateAttributes[1] < generateAttributes[3] && generateAttributes[4] > 0)||
            generateAttributes[4] > generateAttributes[0])
        return false;

    return true;
}

QString ProcessController::getGenerateErrorMessage()
{
    if (generateAttributes[0] <= 0)
        return QString("Can not create less than 1 process. Change amount!");
    if (generateAttributes[0] > generateAttributes[1])
        return QString("Max value must be greater than the amount of processes!");
    if (generateAttributes[2] == 0)
        return QString("Quantum must be greater than 0!");
    if (generateAttributes[1] < generateAttributes[2] && generateAttributes[4] > 0)
        return QString("RR Quantum must be smaller than max value to create conflicts!");
    if (generateAttributes[1] < generateAttributes[3] && generateAttributes[4] > 0)
        return QString("LS Quantum must be smaller than max value to create conflicts!");
    if (generateAttributes[4] > generateAttributes[0])
        return QString("Can not create more conflicts than processes. Increase amount or reduce conflicts!");

    return QString("Error is not defined");
}

bool ProcessController::isGeneratedListValid()
{
    return !procList.empty();
}

bool ProcessController::isSingleSolution()
{
    return singleSolution;
}

void ProcessController::setSingleSolution(bool state)
{
    singleSolution = state;
    generateAttributes[4] = state ? 1 : 0;
}

bool ProcessController::isErrorActive()
{
    return errorActive;
}

QString ProcessController::getErrorHeading()
{
    return errorHeading;
}

QString ProcessController::getErrorDescription()
{
    return errorDescription;
}

void ProcessController::setError(bool state)
{
    errorActive = state;
    emit dataChanged();
}

void ProcessController::setErrorHeading(QString heading)
{
    errorHeading = heading;
}

void ProcessController::setErrorDescription(QString description)
{
    errorDescription = description;
}

void ProcessController::appendProc()
{
    emit preProcAppend();

    process_t proc;
    proc.id = maxProcId;
    proc.arrival = 0;
    proc.duration = 0;
    proc.priority = 0;
    proc.tickets = 0;
    proc.quantum_usage = 0;
    procList.append(proc);
    maxProcId++;
    emit postProcAppend();
    emit dataChanged();

}

void ProcessController::removeProc(int index)
{
    if (procList.size() == 1)
        maxProcId = 0;

    else if (procList.at(index).id == maxProcId - 1)
    {
        int tmp_max = 0;
        for (process_t proc : procList)
            if (proc.id > tmp_max && proc.id != maxProcId - 1)
                tmp_max = proc.id;
        maxProcId = tmp_max + 1;
    }

    emit preProcRemove(index);

    procList.removeAt(index);

    emit postProcRemove();
    emit dataChanged();

}

QString ProcessController::getCharId(unsigned short id)
{
    char base = 65;
    if (id < 26)
        return QString((base+=id));

    else if (id < 26*26 + 26)
    {
        char first = base + id/26 - 1;
        char second = base + (id%26);
        return QString(first) + QString(second);
    }

    else
    {
        char first = base + (id/26)/26 - 1;
        char second = base + ((id/26)%26 - 1);
        char third = base + id%26;
        return QString(first) + QString(second) + QString(third);
    }
}

void ProcessController::setAllButtons()
{
    for (int i=0; i < SCHEDULE_NUM; i++)
    {
        selectedAlgorithms[i] = true;
    }
}

void ProcessController::clearAllButtons()
{
    for (int i=0; i < SCHEDULE_NUM; i++)
    {
        selectedAlgorithms[i] = false;
    }
}

void convert_metrics(metric_t metricList, proc_result_t **resultItem)
{
    proc_result_t *resultItemPtr = *resultItem;
    QVector<proc_metric_t> procMetricList;
    proc_metric_t *it = metricList.proc_metric_ptr;
    resultItemPtr->avg_turnaround_time = metricList.avg_turnaround_time;
    resultItemPtr->avg_response_time = metricList.avg_response_time;
    resultItemPtr->avg_wait_time = metricList.avg_wait_time;
    while (it != nullptr)
    {
        procMetricList.append(*it);
        it = it->next;
    }
    resultItemPtr->procMetric = procMetricList;
    clear_metrics(metricList.proc_metric_ptr);
}

void convertAndCalc(int scheduleType, process_list_t *input_proc_list, proc_result_t *new_results)
{
    convertAndCalc(scheduleType, input_proc_list, new_results, 0, false);
}

void convertAndCalc(int scheduleType, process_list_t *input_proc_list, proc_result_t *new_results, int quantum, bool forceInterruption)
{
    event_gap_list_t newEventList;
    event_list_head_tail_t *scheduledList = nullptr;
    event_list_t *it = nullptr;
    process_list_t *it2 = nullptr;

    new_results->scheduleAlgorithm = scheduleType;

    if (scheduleType == RR || scheduleType == LS || scheduleType == LS_theoretical)
    {
        if (quantum == 0)
            return;
        scheduledList = scheduling(scheduleType, input_proc_list, quantum, forceInterruption);
    }
    else
        scheduledList = scheduling(scheduleType, input_proc_list);

    if (scheduledList == nullptr)
        return;

    convert_metrics(calc_metrics(scheduledList), &new_results);
    it = scheduledList->head;

    while (it != nullptr)
    {
        new_results->resultList.append(*it);
        newEventList.timestamp = it->timestamp;

        if (it->running_process != nullptr)
        {
            newEventList.runningProc = it->running_process->process;
            newEventList.existsRunningProc = true;
        }
        else
            newEventList.existsRunningProc = false;

        it2 = it->process_queue;
        while (it2 != nullptr)
        {
            if (it2->process->id != it->running_process->process->id)
                newEventList.event_list.append(it2->process);
            it2 = it2->next;
        }

        new_results->eventList.append(newEventList);
        newEventList.event_list.clear();

        it = it->next;
    }
    event_list_clear(scheduledList);
}
