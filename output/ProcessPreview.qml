import QtQuick 2.0
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import QtQuick.Dialogs 1.0

import Process 1.0


ListView {
    id: processPreviewView
    anchors.left: parent.left
    anchors.leftMargin: 5
    anchors.top: parent.top
    anchors.topMargin: 5
    anchors.right: parent.right
    anchors.rightMargin: 5
    clip: true

    orientation: Qt.Horizontal

    property int requieredWidth: (processController.getMaxTime()) * 60

    signal clicked()

    Connections {
        target: processController
        onDataChanged: {
            processPreviewView.model = processController.getMaxTime()
            requieredWidth = (processController.getMaxTime()) * 60
        }
    }

    model: processController.getMaxTime()
    spacing: 0
    delegate:
        Item {
        property int procIndex: index
        width: 60
        height: parent.height
        Rectangle {
            id: processFrame
            width: 60
            height: 72

            Rectangle {
                width: 2
                height: parent.height
                border.color: "#828282"
                border.width: 1
            }

            Rectangle {
                width: parent.width
                height: 2
                border.color: "#828282"
                border.width: 1
                anchors.bottom: parent.bottom
            }

            GridView {
                id: processItem
                anchors.fill: parent
                anchors.leftMargin: 2
                anchors.rightMargin: 1
                anchors.bottomMargin: 3
                property int itemIndex: index
                property var numberOfProcs: processController.getProcAmountAtPos(index)
                cellWidth: parent.width-2
                cellHeight: numberOfProcs < 5 ? parent.height/numberOfProcs : parent.height/4
                clip: true

                Connections {
                    target: processController
                    onDataChanged: {
                        processItem.numberOfProcs = processController.getProcAmountAtPos(index)
                        processItem.model = processItem.numberOfProcs
                    }
                }
                model: processController.getProcAmountAtPos(index)
                delegate:
                    Rectangle {
                    id: itemWindow

                    property int itemIndex: index
                    property int procId: processController.getProcId(processItem.itemIndex, index)

                    width: processItem.cellWidth
                    height: processItem.cellHeight
                    color: processController.getColorById(procId)
                    border.color: "#eeeeee"
                    border.width: 0.5

                    OwnText {
                        anchors.centerIn: parent
                        text: "P"+procId
                        font.pixelSize: 11
                        color: "#E0E0E0"
                    }

                    MouseArea {
                        id: itemMouseArea
                        anchors.fill: parent
                        cursorShape: Qt.PointingHandCursor
                        onClicked: {
                            colorPicker.procId = itemWindow.procId
                            colorPicker.currentColor = processController.getColorById(itemWindow.procId)
                            colorPicker.visible = true
                        }
                    }
                }
            }
            OwnText {

                text: "↓";
                anchors.right: parent.right
                anchors.rightMargin: 2
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 2
                font.pixelSize: 22
                font.bold: true
                color: "#ffffff"
                visible: processItem.numberOfProcs < 5 ? false : true
            }
        }


        OwnText {
            text: index;
            anchors.top: processFrame.bottom
            anchors.topMargin: 1
            anchors.left: processFrame.left
            font.pixelSize: 10
            color: "#323232"
        }
    }
    ScrollBar.horizontal: ScrollBar {
        active: true
        policy: processPreviewView.requieredWidth < processPreviewView.width ? ScrollBar.AlwaysOff : ScrollBar.AlwaysOn
    }

    ColorDialog {
        id: colorPicker
        property int procId: 0
        title: qsTr("Please choose a color for process "+procId)
        onAccepted: {
            processController.assignColor(procId, color);
            colorPicker.visible = false
            processPreviewView.model = 0
            processPreviewView.model = processController.getMaxTime()
            processPreviewView.clicked()
        }
        onRejected: colorPicker.visible = false;
    }
}
