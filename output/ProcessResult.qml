import QtQuick 2.0
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

import Process 1.0

// schedule result window
ListView {
    id: processResultView
    width: parent.width
    height: processController.getNumScheduleResults() * 160
    anchors.topMargin: 5
    clip: true

    Connections {
        target: calcButton
        onClicked: {
            processResultView.model = 0
            processResultView.model = processController.getNumScheduleResults()
            processResultView.height = processController.getNumScheduleResults() * 180
        }
    }

    signal clicked()

    model: processController.getNumScheduleResults()

    spacing: 10
    delegate:
        Item {
        id: resultItem
        width: processResultView.width
        height: 160

        Rectangle {
            id: resultArea
            property int scheduleIndex: index
            width: parent.width - 20
            height:130
            anchors.horizontalCenter: parent.horizontalCenter
            border.color: "#828282"
            border.width: 1

            OwnText {
                id: scheduleHeading
                text: processController.getScheduleType(index)
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.top: parent.top
                anchors.topMargin: 5
                font.pixelSize: 20
                color: "#525252"
            }

            Rectangle {
                id: lsHelp
                width: 14
                height: 14
                radius: 7
                border.width: 1
                border.color: "#828282"
                anchors.left: scheduleHeading.right
                anchors.leftMargin: 2
                anchors.top: scheduleHeading.top
                anchors.topMargin: 5
                visible: processController.isLS(index)

                Text {
                    id: lsHelpText
                    anchors.centerIn: parent
                    text: "?"
                    font.pixelSize: 8
                    color: "#828282"
                }

                MouseArea {
                    id: lsHelpMouse
                    anchors.fill: parent
                    hoverEnabled: true
                }

                Hint {
                    width: 280
                    height: 20
                    anchors.bottom: lsHelp.bottom
                    anchors.bottomMargin: -2
                    anchors.left: lsHelpText.right
                    anchors.leftMargin: 5
                    text: processController.isLST(index) ? "Process selection by amount of tickets" : "Process selection by random value"
                    visible: lsHelpMouse.containsMouse
                }
            }

            // Timeline
            ListView {
                id: processResultTimeline
                implicitHeight: 100
                implicitWidth: parent.width - 20
                anchors.bottom: resultArea.bottom
                anchors.bottomMargin: -5
                anchors.left: resultArea.left
                anchors.leftMargin: 5

                clip: true
                orientation: Qt.Horizontal

                Connections {
                    target: calcButton
                    onClicked: {
                        processResultTimeline.model = processController.getMaxSchedulePos(resultArea.scheduleIndex)
                        timelineScroll.policy = processController.getMaxSchedulePos(resultArea.scheduleIndex) * 30 < resultArea.width ? ScrollBar.AlwaysOff : ScrollBar.AlwaysOn
                    }
                }

                model: processController.getMaxSchedulePos(resultArea.scheduleIndex)

                delegate:

                    // time slice
                    Rectangle {
                    id: processFrame
                    property int processIndex: index
                    width: 30
                    height: 50
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: 20

                    ListView {
                        id: processInTimeline
                        implicitHeight: 50
                        implicitWidth: parent.width
                        anchors.left: timelineBottom.left
                        anchors.bottom: timelineBottom.bottom
                        anchors.bottomMargin: -10

                        Connections {
                            target: calcButton
                            onClicked: processInTimeline.model = processController.getProcAmountAtResultPos(resultArea.scheduleIndex, index);
                        }

                        model: processController.getProcAmountAtResultPos(resultArea.scheduleIndex, index)

                        delegate:
                            Rectangle {
                            id: itemWindow
                            property int processId: processController.getProcIdAtResultPos(resultArea.scheduleIndex, processFrame.processIndex)
                            width: processFrame.width
                            height: 40
                            color: procMouseArea.containsMouse ? Qt.lighter(processController.getColorById(processId), 1.5) : processController.getColorById(processId)

                            Connections {
                                target: calcButton
                                onClicked: itemWindow.processId = processController.getProcIdAtResultPos(resultArea.scheduleIndex, processFrame.processIndex)
                            }

                            OwnText {
                                anchors.centerIn: parent
                                text: "P"+itemWindow.processId
                                color: "#E0E0E0"
                            }

                            MouseArea {
                                id: procMouseArea
                                anchors.fill: parent
                                cursorShape: Qt.PointingHandCursor
                                hoverEnabled: true
                                onClicked: {
                                    processController.setMetricAttributes(resultArea.scheduleIndex, itemWindow.processId, processFrame.processIndex)
                                    processController.setClickPosition(resultItem.y)
                                    processResultView.clicked()
                                }
                            }
                        }


                    }

                    // left timeline
                    Rectangle {
                        border.color: "#cfcfcf"
                        border.width: 0.5
                        width: 2
                        height: 45
                        anchors.bottom: timelineBottom.top
                    }

                    // bottom timeline
                    Rectangle {
                        id: timelineBottom
                        border.color: "#828282"
                        border.width: 0.5
                        width: parent.width
                        height: 2
                        anchors.bottom: parent.bottom
                        anchors.bottomMargin: 30
                    }

                    OwnText {
                        text: index;
                        font.pixelSize: 10
                        color: "#323232"
                        anchors.top: timelineBottom.bottom
                        anchors.topMargin: 1
                        anchors.left: timelineBottom.left
                    }
                }

                ScrollBar.horizontal: ScrollBar {
                    id: timelineScroll
                    anchors.bottom: parent.bottom
                    policy: processController.getMaxSchedulePos(resultArea.scheduleIndex) * 30 < resultArea.width ? ScrollBar.AlwaysOff : ScrollBar.AlwaysOn
                }
            }

            //Metrics
            Item {
                id: metricColumn1
                width: resultArea.width/3
                height: 20
                anchors.bottom: resultArea.bottom
                anchors.bottomMargin: 5

                Connections {
                    target: calcButton
                    onClicked: {
                        avgResponseTime.text = "Ø" + qsTr(" Response time: ") + processController.getAvgMetric(ProcessModel.RESPONSE_TIME, resultArea.scheduleIndex)
                        avgWaitTime.text = "Ø" + qsTr(" Waiting time: ") + processController.getAvgMetric(ProcessModel.WAIT_TIME, resultArea.scheduleIndex)
                        avgTurnaroundTime.text = "Ø" + qsTr(" Turnaround time: ") + processController.getAvgMetric(ProcessModel.TURNAROUND_TIME, resultArea.scheduleIndex)
                    }
                }

                Text {
                    id: avgResponseTime
                    text: "Ø" + qsTr(" Response time: ") + processController.getAvgMetric(ProcessModel.RESPONSE_TIME, resultArea.scheduleIndex)
                    color: "#525252"
                    font.family: "Ubuntu"
                    font.pixelSize: 13
                    anchors.centerIn: parent
                }

                Rectangle {
                    id: responseHelp
                    width: 14
                    height: 14
                    radius: 7
                    border.width: 1
                    border.color: "#828282"
                    anchors.left: avgResponseTime.right
                    anchors.leftMargin: 5
                    anchors.bottom: avgResponseTime.bottom
                    anchors.bottomMargin: 1

                    Text {
                        anchors.centerIn: parent
                        text: "?"
                        font.pixelSize: 8
                        color: "#828282"
                    }

                    MouseArea {
                        id: responseHelpMouse
                        anchors.fill: parent
                        hoverEnabled: true
                    }

                    Hint {
                        width: 250
                        height: 20
                        anchors.bottom:  responseHelp.top
                        anchors.horizontalCenter: responseHelp.horizontalCenter
                        text: "Average time from arrival until start"
                        visible: responseHelpMouse.containsMouse
                    }
                }

            }

            Item {
                id: metricColumn2
                width: resultArea.width/3
                height: 20
                anchors.bottom: resultArea.bottom
                anchors.bottomMargin: 5
                anchors.left: metricColumn1.right

                Text {
                    id: avgWaitTime
                    text: "Ø" + qsTr(" Waiting time: ") + processController.getAvgMetric(ProcessModel.WAIT_TIME, resultArea.scheduleIndex)
                    color: "#525252"
                    font.family: "Ubuntu"
                    font.pixelSize: 13
                    anchors.centerIn: parent
                }

                Rectangle {
                    id: waitHelp
                    width: 14
                    height: 14
                    radius: 7
                    border.width: 1
                    border.color: "#828282"
                    anchors.left: avgWaitTime.right
                    anchors.leftMargin: 5
                    anchors.bottom: avgWaitTime.bottom
                    anchors.bottomMargin: 1

                    Text {
                        anchors.centerIn: parent
                        text: "?"
                        font.pixelSize: 8
                        color: "#828282"
                    }

                    MouseArea {
                        id: waitHelpMouse
                        anchors.fill: parent
                        hoverEnabled: true
                    }

                    Hint {
                        width: 200
                        height: 20
                        anchors.bottom:  waitHelp.top
                        anchors.horizontalCenter: waitHelp.horizontalCenter
                        text: "Average time in the queue"
                        visible: waitHelpMouse.containsMouse
                    }
                }
            }

            Item {
                id: metricColumn3
                width: resultArea.width/3
                height: 20
                anchors.bottom: resultArea.bottom
                anchors.bottomMargin: 5
                anchors.left: metricColumn2.right

                Text {
                    id: avgTurnaroundTime
                    text: "Ø" + qsTr(" Turnaround time: ") + processController.getAvgMetric(ProcessModel.TURNAROUND_TIME, resultArea.scheduleIndex)
                    color: "#525252"
                    font.family: "Ubuntu"
                    font.pixelSize: 13
                    anchors.centerIn: parent
                }

                Rectangle {
                    id: turnaroundHelp
                    width: 14
                    height: 14
                    radius: 7
                    border.width: 1
                    border.color: "#828282"
                    anchors.left: avgTurnaroundTime.right
                    anchors.leftMargin: 5
                    anchors.bottom: avgTurnaroundTime.bottom
                    anchors.bottomMargin: 1

                    Text {
                        anchors.centerIn: parent
                        text: "?"
                        font.pixelSize: 8
                        color: "#828282"
                    }

                    MouseArea {
                        id: turnaroundHelpMouse
                        anchors.fill: parent
                        hoverEnabled: true
                    }

                    Hint {
                        width: 250
                        height: 20
                        anchors.bottom:  turnaroundHelp.top
                        anchors.right: parent.right
                        text: "Average time from arrival until completion"
                        visible: turnaroundHelpMouse.containsMouse
                    }
                }
            }
        }
    }
 }
