import QtQuick 2.0
import QtQuick.Controls 2.2

Dialog {
    id: procMetricDialog
    visible: false
    // title: "Attributes"
    width: 300
    height: 200

    modal: true
    x: parent.width/2-150
    y: processController.getClickPosition()

    leftPadding: 0
    topPadding: 0
    rightPadding: 0
    bottomPadding: 0

    signal clicked()

    property alias runningColor: procMetricView.color
    property alias runningModel: queueView.model
    property alias runningId: procIdText.text
    property alias runningPosition: positionText.text
    property alias runningRespone: responseTime.text
    property alias runningWait: waitTime.text
    property alias runningTurnaround: turnaroundTime.text
    property alias runningVisible: procMetricDialog.visible

    //generate random processes window
    Rectangle {
        id: procMetricView
        width: parent.width
        height: parent.height/2
        anchors.bottomMargin: 0
        color: "#ffffff"
        border.color: "#828282"
        border.width: 1

        Text {
            id: procIdText
            text: qsTr("Process ") + qsTr("ID: ") + "P"+processController.getMetricId()
            font.family: "Ubuntu"
            font.pixelSize: 16
            color: "#ffffff"
            anchors.top: parent.top
            anchors.topMargin: parent.height/6
            anchors.left: parent.left
            anchors.leftMargin: 10
        }

        Text {
            id: positionText
            text: qsTr("Timestamp")+ ": " + processController.getMetricPos()
            font.family: "Ubuntu"
            font.pixelSize: 16
            color: "#ffffff"
            anchors.bottom: parent.bottom
            anchors.bottomMargin: parent.height/6
            anchors.left: parent.left
            anchors.leftMargin: 10
        }

        Text {
            id: responseTime
            text: qsTr("Response time: ") + processController.getMetricResponse()
            font.family: "Ubuntu"
            font.pixelSize: 14
            color: "#ffffff"
            anchors.right: parent.right
            anchors.rightMargin: 10
            anchors.top: parent.top
            anchors.topMargin: 20
        }

        Text {
            id: waitTime
            text: qsTr("Waiting time: ") + processController.getMetricWait()
            font.family: "Ubuntu"
            font.pixelSize: 14
            color: "#ffffff"
            anchors.right: parent.right
            anchors.rightMargin: 10
            anchors.top: responseTime.top
            anchors.topMargin: 20
        }

        Text {
            id: turnaroundTime
            text: qsTr("Turnaround time: ") + processController.getMetricTurnaround()
            font.family: "Ubuntu"
            font.pixelSize: 14
            color: "#ffffff"
            anchors.right: parent.right
            anchors.rightMargin: 10
            anchors.top: waitTime.top
            anchors.topMargin: 20
        }
    }

    // queue at pos
    Rectangle {
        id: procQueueView
        implicitWidth: parent.width
        implicitHeight: parent.height/2
        anchors.top: procMetricView.bottom
        border.color: "#828282"
        border.width: 1

        Text {
            id: procQueueText
            text: qsTr("Queue")
            font.family: "Ubuntu"
            font.pixelSize: 14
            anchors.horizontalCenter: queueView.horizontalCenter
            anchors.bottom: queueView.top
            anchors.bottomMargin: 2
            anchors.left: queueView.left
        }


        ListView {
            id: queueView
            anchors.left: parent.left
            anchors.leftMargin: 10
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 10
            height: parent.height/2 + parent.height/6
            width: parent.width/2 + parent.width/8
            clip: true

            orientation: ListView.Horizontal
            layoutDirection: Qt.RightToLeft

            Connections {
                target: procMetricDialog
                onClicked: {
                    queueView.model = 0
                    queueView.model = processController.getNumQueue()
                }
            }

            model: processController.getNumQueue()
            delegate:

                Rectangle {
                id: processFrame
                property int processIndex: index
                property int metricAtIndex: processController.getMetricAt(index)
                width: 50
                height: parent.height - 5
                border.color: "#828282"
                border.width: 1

                Connections {
                    target: resultBorder
                    onClicked: {
                        processFrame.metricAtIndex = processController.getMetricAt(index)
                        processFrame.color = processController.getColorById(processFrame.metricAtIndex)
                    }
                }

                color: processController.getColorById(metricAtIndex)

                Text {
                    id: queueProcId
                    anchors.centerIn: parent
                    text: processFrame.metricAtIndex == -1 ? "" : "P"+processFrame.metricAtIndex
                    font.family: "Ubuntu"
                    font.pixelSize: 12
                    color: "#e0e0e0"
                }

            }
            ScrollBar.horizontal: ScrollBar {
                policy: parent.model < 4 ? ScrollBar.AlwaysOff : ScrollBar.AlwaysOn
                anchors.bottom: parent.bottom
            }
        }

        // line between queue and running
        Rectangle {
            width: 20
            height: 2
            color: "#000000"
            anchors.verticalCenter: runningProcView.verticalCenter
            anchors.right: runningProcView.left
            anchors.rightMargin: 5
        }


        // running proc
        Text {
            id: runningProcText
            text: qsTr("Running")
            font.family: "Ubuntu"
            font.pixelSize: 14
            anchors.horizontalCenter: runningProcView.horizontalCenter
            anchors.bottom: runningProcView.top
            anchors.bottomMargin: 2
        }

        Rectangle {
            id: runningProcView
            implicitHeight: parent.height/2 + parent.height/6
            implicitWidth: 50
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 10
            anchors.right: parent.right
            anchors.rightMargin: 10
            border.color: "#828282"
            border.width: 1

            Connections {
                target: procMetricDialog
                onClicked: {
                    runningProcView.color = processController.getColorById(processController.getMetricId())
                    runningProcId.text = "P"+processController.getMetricId()
                }
            }

            color: processController.getColorById(processController.getMetricId())

            Text {
                id: runningProcId
                text: "P"+processController.getMetricId()
                anchors.centerIn: parent
                font.family: "Ubuntu"
                font.pixelSize: 12
                color: "#e0e0e0"
            }
        }
    }
}
